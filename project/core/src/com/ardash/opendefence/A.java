/*******************************************************************************
 * Copyright (C) 2015-2017 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence;

import java.util.EnumSet;

import com.ardash.opendefence.screens.GenericScreen;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader.ParticleEffectParameter;
import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Logger;

/**
 * This is the Asset class from the Game Apple Flinger. It is very robust and convenient.
 * @author Andreas Redmer
 *
 */
public class A {

	private static final String RUSSIAN_CHARACTERS = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
            + "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"
            + "1234567890.,:;_¡!¿?\"'+-*/()[]={}";
	
	private static final String UKRAINIAN_CHARACTERS = "АаБбВвГгҐґДдЕеЄєЖжЗзИиІіЇїЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЬьЮюЯя"
			+ "ЙЦУКЕНГШЩЗХЇҐФІВАПРОЛДЖЄЯЧСМИТЬБЮ"
			+ "йцукенгшщзхїґфівапролджєячсмитьбю"
            + "1234567890.,:;_¡!¿?\"'+-*/()[]={}";
	
	private static final String POLISH_CHARACTERS = "ABCDEFGHIJKLMNOPRSTUVWYZĄĆĘŁŃÓŚŹŻ"
            + "abcdefghijklmnoprstuvwyząćęłńóśźż"
            + "1234567890.,:;_¡!¿?\"'+-*/()[]={}";
	
	private static final String EO_CHARACTERS = "ĉĝĥĵŝŭĈĜĤĴŜŬ";
	
	private static final String EXTRA_CHARACTERS = RUSSIAN_CHARACTERS + POLISH_CHARACTERS + EO_CHARACTERS + UKRAINIAN_CHARACTERS;

	public enum TextButtonStyleAsset {
		MAP_SELECT;
		
		public TextButtonStyle style;
		static {
			MAP_SELECT.style = new TextButtonStyle();
			MAP_SELECT.style.font = A.FontAsset.ZANTROKE_MAP_SELECT.font;
			MAP_SELECT.style.fontColor=Color.WHITE;
		}
	}
	
	public enum LabelStyleAsset {
		MAP_SELECT, HEADLINE;
		
		public LabelStyle style;
		static {
			MAP_SELECT.style = new LabelStyle();
			MAP_SELECT.style.font = A.FontAsset.ZANTROKE_MAP_SELECT.font;
			MAP_SELECT.style.fontColor=Color.WHITE;

			HEADLINE.style = new LabelStyle();
			HEADLINE.style.font = A.FontAsset.HEADLINE_216.font;
			HEADLINE.style.fontColor=Color.WHITE;
//			HEADLINE.style.fontColor=Color.BLACK;

		}
	}
	
	public enum FontAsset {
		HEADLINE_216, HEADLINE_75, 
		FLINGER_09_B5_POINT_POP,
		FLINGER_05_B4_BIGMENU, 
		FLINGER_03_B2_DIAG_MINIL,
		ZANTROKE_MAP_SELECT,
		ZANTROKE_03_B2_DIAG_MINIL_CYRILLIC;
		
		public BitmapFont font;
		// init
		static {
			final float FONT_SIZE_LARGE_20 = 0.1f * GenericScreen.SCREEN_HEIGHT;
			float FONT_SIZE_MEDIUM_09 = 0.09f * GenericScreen.SCREEN_HEIGHT;
			float FONT_SIZE_MEDIUM_05 = 0.025f * GenericScreen.SCREEN_HEIGHT;
			float FONT_SIZE_SMALL_03 = 0.03f * GenericScreen.SCREEN_HEIGHT;
			{ //Headline
				FreeTypeFontGenerator generator;
				FreeTypeFontParameter parameter;
				generator = A.getFontGenerator(FontGeneratorAsset.HEADLINE3);
				parameter = defaultParameter((int)Math.ceil(FONT_SIZE_LARGE_20),0.1f);
				parameter.borderColor = Color.BLACK;
				parameter.borderWidth = 8;
				generator.scaleForPixelHeight((int)Math.ceil(FONT_SIZE_LARGE_20));
	
				HEADLINE_216.font = generator.generateFont(parameter);
				HEADLINE_216.font.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
			}
			
			{ //Zantroke
				FreeTypeFontGenerator generator;
				FreeTypeFontParameter parameter;
				generator = A.getFontGenerator(FontGeneratorAsset.ZANTROKE); // ***************
				parameter = defaultParameter((int)Math.ceil(FONT_SIZE_MEDIUM_05),4f);
				generator.scaleForPixelHeight((int)Math.ceil(FONT_SIZE_MEDIUM_05));
				parameter.characters = FreeTypeFontGenerator.DEFAULT_CHARS+EXTRA_CHARACTERS;
				ZANTROKE_MAP_SELECT.font = generator.generateFont(parameter);
				ZANTROKE_MAP_SELECT.font.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);

//				parameter = defaultParameter((int)Math.ceil(FONT_SIZE_SMALL_03*.81f),2f);
//				generator.scaleForPixelHeight((int)Math.ceil(FONT_SIZE_SMALL_03*.81f));
//				parameter.characters = FreeTypeFontGenerator.DEFAULT_CHARS+EXTRA_CHARACTERS;
//				ZANTROKE_03_B2_DIAG_MINIL_CYRILLIC.font = generator.generateFont(parameter);
//				ZANTROKE_03_B2_DIAG_MINIL_CYRILLIC.font.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
			}

		}

		/* extracted method to save some lines, returns some default params for fonts */
		private static FreeTypeFontParameter defaultParameter(int size, float borderWidth) {
			FreeTypeFontParameter parameter = new FreeTypeFontParameter();
			parameter.minFilter = Texture.TextureFilter.Nearest;
			parameter.magFilter = Texture.TextureFilter.MipMapLinearNearest;
			parameter.borderColor = Color.BLACK;
			parameter.borderStraight=false;
			parameter.borderWidth=borderWidth;
			parameter.size=size;
			return parameter;
		}
		
		@Override
		public String toString() {
			return "size" + super.toString().replaceAll("[^\\d.]", "") + ".ttf"; // "size72.ttf"    
		}
	}

	private enum FontGeneratorAsset {
		HEADLINE3, FLINGER, ZANTROKE;
		@Override
		public String toString() {
			return "" + super.toString().toLowerCase() + ".ttf"; // example "fliger.ttf"  
		}
	}

	
	/**
	 * skin is not an enum because there is only one
	 */
	public static final AssetDescriptor<Skin> SKIN = new AssetDescriptor<>("shade/uiskin.json", Skin.class);
	
	public enum ParticleAsset {
		ICE_ADDITIVE,FIRE_ADDITIVE6;
		@Override
		public String toString() {
			return "" + super.toString().toLowerCase() + ".p";
		}
	}

	public enum TextureAsset {
		MENUBACK;
		@Override
		public String toString() {
			return "" + super.toString().toLowerCase() + ".jpg";
		}
	}

	public enum MusicAsset {
		INDUSTRIAL_96;
		@Override
		public String toString() {
			return "music/" + super.toString().toLowerCase() + ".mp3";
		}
	}

	/**
	 * Sound asset (for single sounds)
	 * 
	 */
	public enum SoundAsset {
		DIRT_EXPLOSION1,DIRT_EXPLOSION2,GATLING1,
		ICE_SPRAY,MISSILE1,MISSILE2,MISSILE3,
		MISSILE_HIT1, MISSILE_HIT2, MISSILE_HIT3, MORTAR1,
		
		SP_TARGET_NEAREST,SP_TARGET_FIRST,SP_TARGET_STRONGEST,SP_TARGET_WEAKEST,SP_TURRET_SOLD,SP_UPGRADE,SP_UPGRADE_COMPLETE;

		@Override
		public String toString() {
			return "sound/" + super.toString().toLowerCase() + ".mp3";
		}
	}
	
// 	misc_atlas = new TextureAtlas(Gdx.files.internal("misc.atlas"));
//	seyes_closed = misc_atlas.createSprite("eyes_closed");
	public enum AtlasAsset {
		ACTORS, EXPLOSION, GUI;
		@Override
		public String toString() {
			return "" + super.toString().toLowerCase() + ".atlas"; // "misc.atlas"  
		}
	}

	/**
	 * AtlasRegionAsset = the name is just too long and spoils the code. 
	 * so renamed it ARAsset
	 * regions on atlasses
	 */
	public enum ARAsset {
		  
		LIGHTNINGSEGMENT, ROCKET, GUN_LVL0_BASE, GUN_LVL2_BASE, GATLING_TOP, TURRETCURSOR,
		FREEZER_TOP,TESLA_BASE,BULLET_HIT,SMOKE_TRACE,

		// regions on ui atlas
		ICON_GATLING,ICON_RPG,ICON_TESLA,ICON_ICE,ICON_2ROCKETS,ICON_MORTAR,ICON_PLAY,ICON_FORWARD,ICON_PAUSE,
		GPL3,FORK_ME,

		// region arrays
		BLOODMARK,BURNMARK, 
		CURSOR, RADAR;
		@Override
		public String toString() {
			return "" + super.toString().toLowerCase() + ""; 
		}
	}
	


	public enum SpriteAsset {
		PUFF_0,PUFF_1,PUFF_2,PUFF_3,PUFF_4,PUFF_5,PUFF_6,PUFF_7,PUFF_8,PUFF_9,
		
		DORK_0,DORK_1,DORK_2,DORK_3,

		PENG_0,
		EYES_CLOSED_PENG,
		
		BIRD_0,BIRD_1,BIRD_2,BIRD_3,BIRD_4,BIRD_5,

		BIRD_6,BIRD_7,BIRD_8,BIRD_9,BIRD_10,BIRD_11,
		
		WOOD_TNT_0,
		WOOD_BL_11_0,WOOD_BL_11_1,WOOD_BL_11_2,WOOD_BL_11_3,
		
		WOOD_BL_21_0,WOOD_BL_21_1,WOOD_BL_21_2,WOOD_BL_21_3,
		WOOD_BL_22_0,WOOD_BL_22_1,WOOD_BL_22_2,WOOD_BL_22_3,
		
		WOOD_BL_41_0,WOOD_BL_41_1,WOOD_BL_41_2,WOOD_BL_41_3,
		WOOD_BL_42_0,WOOD_BL_42_1,WOOD_BL_42_2,WOOD_BL_42_3,
		
		WOOD_BL_81_0,WOOD_BL_81_1,WOOD_BL_81_2,WOOD_BL_81_3,
		
		WOOD_TRIA_0,WOOD_TRIA_1,WOOD_TRIA_2,WOOD_TRIA_3,
		WOOD_RECT_0,WOOD_RECT_1,WOOD_RECT_2,WOOD_RECT_3,
		
		EYES_CLOSED,
		EYES_DOWN,
		EYES_INNER,
		EYES_LEFT,
		EYES_OUTER,
		EYES_RIGHT,
		EYES_UP,

		DIALOG,
		SLIDERBACK,
		FLAG_DE,
		FLAG_EN,
		FLAG_EO,
		FLAG_ES,
		FLAG_FR,
		FLAG_PL,
		FLAG_RU,
		BTN_INFO,
		BTN_JOYPAD,
		BTN_LEADER,
		BTN_ACHI,
		BTN_SQ_EMPTY,
		BTN_FL_EMPTY,
		BTN_TW,
		BTN_FB,
		BTN_GP,
		BTN_PI,
		BTN_CLOSE,
		BTN_PLAY,
		BTN_1PLAYER,
		BTN_2PLAYERS,
		BTN_SETTINGS,
		BTN_SOUND_ON,
		BTN_SOUND_OFF,
		BTN_BACK,
		BTN_PAUSE,
		BTN_REFRESH,
		BTN_ABORT,
		BTN_BLANK,
		BTN_WORLD;
		
		static {
			// init all sprites
			for (SpriteAsset e : SpriteAsset.values()) {
				// first the name and index must be set, so thString(works properly)
				final String lowername = e.name().toLowerCase();
				if (lowername.contains("_")) 
				{
					final int uscp = lowername.lastIndexOf('_'); // underscore position
					e.rname = lowername.substring(0, uscp);
					final String lIndex = lowername.substring(uscp+1, lowername.length());
					try {
						e.rindex = Integer.valueOf(lIndex);
					} catch (NumberFormatException e1) {
						e.rname = lowername;
						e.rindex = -1;
					}
				}
				else
				{
					e.rname = lowername;
					e.rindex = -1;
				}
				
				// the sprite could be found automatically here, by searching all regions in the atlas for that name
				// but then all atlasses would have to be preloaded right away as soon as only one sprite is needed. is that good?
				
				// try it
				TextureAtlas foundAtlas = null;
				for (AtlasAsset aa : AtlasAsset.values())
				{
					final TextureAtlas atlas = getAtlas(aa);
					final AtlasRegion foundRegion = atlas.findRegion(e.rname,e.rindex);
					if (foundRegion!= null)
					{
						foundAtlas = atlas;
						if (foundRegion.index !=e.rindex)
							throw new RuntimeException("found region "+ e.toString() + " but with wrong index");  
						break;
					}
				}
				if (foundAtlas == null)
					throw new RuntimeException("No atlas found for "+e.toString()); 
				foundAtlas.getTextures().first().setFilter(TextureFilter.Linear, TextureFilter.Linear);
				e.sprite= foundAtlas.createSprite(e.rname,e.rindex);
				if (e.sprite == null)
					throw new RuntimeException("Sprite not created for "+e.toString()); 
			}
		}

		private Sprite sprite;
		private String rname =""; 
		private int rindex=-1;
		public Sprite get()
		{
			return sprite;
		}
		
		@Override
		public String toString() {
			return "" + rname + (rindex ==-1 ? "" : "_"+rindex); // "eyes_closed"   
		}
	}


	/**
	 * Sound group
	 *
	 */
	public enum SoundGroupAsset {
		RUBBER, WHIZZ, EXPLOSION, 
		
		DORK_HIT,

		APPLE_HIT, APPLE_DEST,
		
		WOOD_HIT, WOOD_DMG, WOOD_DEST;

		static {
			for (SoundGroupAsset e : SoundGroupAsset.values()) {
				if (e.members.size() == 0) {
					// apply default format - get format from own name
					final String format = e.name() + "_%d"; 
					e.fillMembersByFormat(format, e.members);
				}
				if (e.members.size() == 0)
					throw new RuntimeException("Empty Asset Group created in " + e.toString()); 
			}
		}

		public final EnumSet<SoundAsset> members; // EnumSet has no efficient way of
												// choosing a random element

		/**
		 * creates the sound group exactly from the specified enum set
		 * 
		 * @param members
		 */
		SoundGroupAsset(EnumSet<SoundAsset> members) {
			if (members == null)
				throw new RuntimeException("members can't be null"); 
			this.members = members;
			if (members.size() == 0)
				throw new RuntimeException("Empty Asset Group created"); 
		}

		/**
		 * creates the sound group by the specified format
		 * 
		 * @param format
		 *            must contain %d ,which will be replaced by a number
		 *            [0,+inf]
		 */
		SoundGroupAsset(final String format) {
			final EnumSet<SoundAsset> result = EnumSet.noneOf(SoundAsset.class);
			fillMembersByFormat(format, result);

			this.members = result;
		}

		/**
		 * @param format
		 *            must contain %d ,which will be replaced by a number
		 *            [0,+inf]
		 * @param result
		 *            the memebrs array
		 */
		private void fillMembersByFormat(final String format, final EnumSet<SoundAsset> result) {
			for (int i = 0; i < SoundAsset.values().length; i++) {
				// get all the SAsset values that match this name structure
				try {
					final SoundAsset newE = SoundAsset.valueOf(String.format(format, i));
					result.add(newE);
				} catch (Throwable t) {
					break;
				}
			}
		}

		/**
		 * creates the members automatically using its own name as format,
		 * happens in a static block after <init>
		 * 
		 * @param format
		 */
		SoundGroupAsset() {
			EnumSet<SoundAsset> result = EnumSet.noneOf(SoundAsset.class);
			this.members = result;
		}

		public SoundAsset getRandom() {
			return (SoundAsset) (members.toArray())[MathUtils.random(0, members.size() - 1)];
		}
	}

	/**
	 * Sound group
	 *
	 */
	public enum SpriteGroupAsset {
		
		WOOD_RECT,
		WOOD_TRIA,
		WOOD_BL_81,
		WOOD_BL_42,
		WOOD_BL_41,
		WOOD_BL_22,
		WOOD_BL_21,
		WOOD_BL_11,
		
		WOOD_TNT,
		PUFF,
		BIRD,
		DORK,
		PENG;

		static {
			for (SpriteGroupAsset e : SpriteGroupAsset.values()) {
				if (e.members.size() == 0) {
					// apply default format - get format from own name
					final String format = e.name() + "_%d"; 
					e.fillMembersByFormat(format, e.members);
				}
				if (e.members.size() == 0)
					throw new RuntimeException("Empty Asset Group created in " + e.toString()); 
			}
		}

		public final EnumSet<SpriteAsset> members;

		/**
		 * creates the sound group by the specified format
		 * 
		 * @param format
		 *            must contain %d ,which will be replaced by a number
		 *            [0,+inf]
		 */
		SpriteGroupAsset(final String format) {
			final EnumSet<SpriteAsset> result = EnumSet.noneOf(SpriteAsset.class);
			fillMembersByFormat(format, result);

			this.members = result;
		}

		/**
		 * @param format
		 *            must contain %d ,which will be replaced by a number
		 *            [0,+inf]
		 * @param result
		 *            the members array
		 */
		private void fillMembersByFormat(final String format, final EnumSet<SpriteAsset> result) {
			for (int i = 0; i < SpriteAsset.values().length; i++) {
				// get all the SAsset values that match this name structure
				try {
					final SpriteAsset newE = SpriteAsset.valueOf(String.format(format, i));
					result.add(newE);
				} catch (Throwable t) {
					break;
				}
			}
		}

		/**
		 * creates the members automatically using its own name as format,
		 * happens in a static block after <init>
		 * 
		 * @param format
		 */
		SpriteGroupAsset() {
			EnumSet<SpriteAsset> result = EnumSet.noneOf(SpriteAsset.class);
			this.members = result;
		}

		public SpriteAsset getRandom() {
			return (SpriteAsset) (members.toArray())[MathUtils.random(0, members.size() - 1)];
		}
		public SpriteAsset get(int i) {
			return (SpriteAsset) (members.toArray())[i];
		}
		public int size() {
			return members.size();
		}
	}

	private static final AssetManager manager = new AssetManager();
	static {
		FileHandleResolver resolver = new InternalFileHandleResolver();
		manager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(resolver));
		manager.setLoader(BitmapFont.class, ".ttf", new FreetypeFontLoader(resolver)); 
		Texture.setAssetManager(manager);
	}
	
	private static Logger log = new Logger("A", Application.LOG_NONE); 
	
	/**
	 * this is the preparation of anync load, so it knows what is supposed to be loaded
	 * for load() it is included
	 */
	public static void enqueueAll() {
		// manager.load(MAsset.BG.toString(), Music.class);
		// can iterate over all enums, but even if not, the
		// get{Music|Sound|Texture} Methods will load it
		manager.load(SKIN);
		for (SoundAsset sa : SoundAsset.values()) {
			manager.load(sa.toString(), Sound.class);
		}
		for (MusicAsset ma : MusicAsset.values()) {
			manager.load(ma.toString(), Music.class);
		}
		for (AtlasAsset aa : AtlasAsset.values()) {
			manager.load(aa.toString(), TextureAtlas.class);
		}
		for (TextureAsset aa : TextureAsset.values()) {
			manager.load(aa.toString(), Texture.class);
		}
	}
	
	// must be called repeatedly, will then load stuff in background
	public static boolean loadAsync() {
		return manager.update();
	}

	public static Music getMusic(MusicAsset m) {
		final String path = m.toString();
		if (!manager.isLoaded(path)) {
			manager.load(path, Music.class);
			manager.finishLoading();
		}
		final Music music = manager.get(path, Music.class);
		music.setLooping(true);
		return music;
	}

	public static Sound getSound(SoundAsset s) {
		final String path = s.toString();
		if (!manager.isLoaded(path)) {
			manager.load(path, Sound.class);
			manager.finishLoading();
		}
		return manager.get(path, Sound.class);
	}

	public static ParticleEffect getParticleEffect(ParticleAsset pa) {
		final String path = pa.toString();
		if (!manager.isLoaded(path)) {
			ParticleEffectParameter p = new ParticleEffectParameter();
			p.atlasFile = AtlasAsset.ACTORS.toString();
			manager.load(path, ParticleEffect.class , p );
			manager.finishLoading();
		}
		return manager.get(path, ParticleEffect.class);
	}

	/**
	 * Direct access (by name) to the image regions of the actors atlas.
	 * This is needed because the enemy texture names are stored in JSON and
	 * are not accesses via source code constant.
	 * 
	 * @param The texture name of an enemy that is mentioned in the enemy-json-description.
	 * @return
	 */
	public static AtlasRegion getTextureRegion(String regionName, boolean finishLoading) {
		if (finishLoading)
		{
			manager.finishLoadingAsset(AtlasAsset.ACTORS.toString());
			manager.finishLoadingAsset(AtlasAsset.GUI.toString());
		}
		
		AtlasRegion found = A.getAtlas(AtlasAsset.ACTORS).findRegion(regionName);
		if (found==null)
			found = A.getAtlas(AtlasAsset.GUI).findRegion(regionName);
		if (found!=null)
			return found;
		throw new RuntimeException(regionName + " was not found on any atlas");
	}
	
	public static AtlasRegion getTextureRegion(ARAsset tra) {
		return getTextureRegion(tra.toString(), false);
	}

	public static Array<AtlasRegion> getTextureRegions(String regionArray) {
		Array<AtlasRegion> found = A.getAtlas(AtlasAsset.ACTORS).findRegions(regionArray);
		if (found!=null)
			return found;
		throw new RuntimeException(regionArray + " was not found on any atlas");
	}
	
	public static Array<AtlasRegion> getTextureRegions(ARAsset tra) {
		return getTextureRegions(tra.toString());
	}


	/**
	 * for textures that are not part of an atlas (usually big ones).
	 * @param s
	 * @return
	 */
	public static Texture getTexture(TextureAsset s) {
//		if (s==TextureAsset.INVISIBLE)
//			return null;
		final String path = s.toString();
		if (!manager.isLoaded(path)) {
			final TextureParameter param = new TextureParameter();
			param.minFilter = TextureFilter.MipMapLinearLinear;
			param.magFilter= TextureFilter.Linear;
			param.genMipMaps = true;
			manager.load(path, Texture.class,param);
			manager.finishLoading();
		}
		final Texture texture = manager.get(path, Texture.class);
		texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		//texture.setFilter(TextureFilter.MipMapLinearNearest, TextureFilter.MipMapLinearLinear);
		return texture;
	}

	public static Sound getRandomSound(SoundGroupAsset sg) {
		final SoundAsset random = sg.getRandom();
		final Sound sound = getSound(random);
		if (sound == null)
			log.error("sound was null"); 
			//throw new RuntimeException("sound was null");
		return sound;
	}

	// TODO make private again?!
	public static TextureAtlas getAtlas(AtlasAsset aa) {
		final String path = aa.toString();
		if (!manager.isLoaded(path)) {
			manager.load(path, TextureAtlas.class);
			manager.finishLoading();
		}
		return manager.get(path, TextureAtlas.class);
	}
	
	private static FreeTypeFontGenerator getFontGenerator(FontGeneratorAsset aa) {
		final String path = aa.toString();
		if (!manager.isLoaded(path)) {
			manager.load(path, FreeTypeFontGenerator.class);
			manager.finishLoading();
		}
		return manager.get(path, FreeTypeFontGenerator.class);
	}


	public static Skin getSkin() {
		if (!manager.isLoaded(SKIN.fileName)) {
			manager.load(SKIN);
			manager.finishLoadingAsset(SKIN.fileName);
		}
		return manager.get(SKIN);
	}


//	/**
//	 * early access all enum, so errors throw up early
//	 */
//	public static void validate() {
//		for (SoundGroupAsset sg : SoundGroupAsset.values()) {
//			sg.toString();
//		}
//	}

	public static void dispose() {
		manager.dispose();
//		for (FontAsset fa : FontAsset.values())
//		{
//			try {
//				fa.font.dispose(); // can throw if already disposed
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
	}
	
	public static int getPercentLoaded()
	{
		return (int)(manager.getProgress()*100);
	}
	

}
