/*******************************************************************************
 * Copyright (C) 2017, 2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence;

import java.util.ArrayList;
import java.util.Iterator;

import com.ardash.opendefence.A.ARAsset;
import com.ardash.opendefence.common.DamageType;
import com.ardash.opendefence.common.Enemy;
import com.ardash.opendefence.common.HasCenter;
import com.ardash.opendefence.common.PoorObjFloatConsumer;
import com.ardash.opendefence.helpers.Spawner;
import com.ardash.opendefence.screens.GameScreen;
import com.ardash.opendefence.screens.gui.UiStage;
import com.ardash.opendefence.turrets.Flame;
import com.ardash.opendefence.turrets.Freezer;
import com.ardash.opendefence.turrets.Rocket;
import com.ardash.opendefence.turrets.Turret;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.Viewport;

public class DefenceStage extends Stage {
	
	public class EnemyContext 
	{
		
	}
	
	/*
	 * Stage Render order:
	 * 
	 * 0 Map
	 * 1 grounded blood
	 * 2 grounded ash
	 * 3 turret
	 * 4 enemy
	 * 5 particle_blood splatter and bullet hits
	 * 6 and hit explosions (extra for performance)
	 * 7 small explosions (extra for performance)
	 * 8 particle_smoke (big particle), fire, ice
	 * 9 rockets and lightnings
	 *10 Trees_and_Bridges
	 *11 particle_explosion (big ones)
	 *12 radar
	 */
	
	private final ArrayList<Enemy> enemies;
	private final ArrayList<Turret> turrets;
	public final Group grp00background = new Group();
	public final Group grp01blood = new Group();
	public final Group grp02ash = new Group();
	public final Group grp03turret = new Group();
	public final Group grp04enemy = new Group();
	public final Group grp05smallParticle = new Group();
	public final Group grp06hitExplosions = new Group();
	public final Group grp07smallExplosions = new Group();
	public final Group grp08bigParticle = new Group();
	public final Group grp09projectiles = new Group();
	public final Group grp10trees = new Group();
	public final Group grp11bigExplosions = new Group();
	public final Group grp12radar = new Group();
	public final Image radar;
	private Image validDropArea;
	public final OpenDefenceGame game;
	public final GameManager gm;
	private Spawner spawner;
	private UiStage uiStage = null;
	private Turret currentSelectedTurret = null;

	public DefenceStage(Viewport viewport, OpenDefenceGame game) {
		super(viewport);
		enemies = new ArrayList<>();
		turrets = new ArrayList<>();
		this.game = game;
		this.gm = game.gm;
	    spawner = new Spawner(this);
	    super.addActor(grp00background);
	    super.addActor(grp01blood);
	    super.addActor(grp02ash);
	    super.addActor(grp03turret);
	    super.addActor(grp04enemy);
	    super.addActor(grp05smallParticle);
	    super.addActor(grp06hitExplosions);
	    super.addActor(grp07smallExplosions);
	    super.addActor(grp08bigParticle);
	    super.addActor(grp09projectiles);
	    super.addActor(grp10trees);
	    super.addActor(grp11bigExplosions);
	    super.addActor(grp12radar);
	    
	    radar = new Image(A.getTextureRegion(ARAsset.RADAR));
	    radar.setVisible(false);
		radar.setPosition(-radar.getWidth()/2, -radar.getHeight()/2 );
		radar.setOrigin(radar.getWidth()/2, radar.getHeight()/2);
		radar.setTouchable(Touchable.disabled);
		grp12radar.addActor(radar);
		
		for (Actor grp : super.getRoot().getChildren()) {
			grp.setTouchable(Touchable.disabled);
		}
		grp03turret.setTouchable(Touchable.enabled);
		grp00background.setTouchable(Touchable.enabled);
	    
	    final Flame flame = new Flame();
		//super.addActor(flame);
//	    grp02ash.addActor(flame);
	    
//	    final IceEffect iceEffect = new IceEffect();
//	    grp08bigParticle.addActor(iceEffect);
//	    
//	    iceEffect.setPosition(100, 200);
	}
	
	@Override
	public void addActor(Actor actor) {
		throw new RuntimeException("Don't call this directly. Please add to one of the stage groups to keep the render order.");
	}
	
	@Override
	public void act() {
		super.act();
		// this must be overridden, to call the act(delta) method below
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);
		spawner.act(delta);
		
		// check if it is time for next wave
		if (spawner.isWaveEmpty()) // cond1: wave empty
		{
			if (enemies.isEmpty()) // cond2: all enemies gone
			{
				if (! gm.isInLastWave()) // cond3: must not be last wave
				{
					gm.startNextWave();
					spawner.triggerNewWave();
				}
				else // if it was last wave: win the game
				{
					// TODO win the game
					Gdx.app.log("DefenceStage", "game won"); 
					
				}
			}
		}
		
	}
	
	public void showRadar(Turret t) {
		// set correct radar size and set visibility
		final float range = t.getRange();
		final float factor = 1.7f;
		radar.setSize(range*factor, range*factor);
		radar.setPosition(t.getCenterX()-radar.getWidth()/2, t.getCenterY()-radar.getHeight()/2);
		radar.setOrigin(radar.getWidth()/2, radar.getHeight()/2);

		radar.setVisible(true);
	}
	
	public void hideRadar() {
		radar.setVisible(false);
	}
	
	public void addTurret(Turret newTurret) {
		turrets.add(newTurret);
		grp03turret.addActor(newTurret);
	}
	
	public void removeTurret(Turret turret) {
		turrets.remove(turret);
		turret.remove();
	}
	
	public void addEnemy (Enemy e)
	{
		enemies.add(e);
//		this.addActor(e);
		grp04enemy.addActor(e);
	}
	
	/**
	 * @param e
	 */
	public void removeEnemy (Enemy e)
	{
		// remove from group
		enemies.remove(e);
		e.remove();
	}
	
	public ArrayList<Enemy> getEnemies()
	{
		return enemies;
	}
	
	public ArrayList<Turret> getTurrets()
	{
		return turrets;
	}
	
	public UiStage getUiStage() {
		if (uiStage == null)
		{
			uiStage = ((GameScreen)game.getScreen()).uiStage;
			uiStage.setGameStage(this);
		}
		return uiStage;
	}

	public void setCurrentSelectedTurret(Turret newSelectedTurret) {
		//always unselect current selection
		if (currentSelectedTurret!=null)
		{
			currentSelectedTurret.setSelected(false);
			currentSelectedTurret=null;
		}
		
		//make new selection (even if null
		this.currentSelectedTurret = newSelectedTurret;
		
		// if new selection was not null, forward the signal
		if (currentSelectedTurret!=null)
		{
			currentSelectedTurret.setSelected(true);
		}
	}

	public float getDistance(HasCenter a1, HasCenter a2)
	{
		Vector2 p1 = P.getVector2().set(a1.getCenterX(),a1.getCenterY());
		final float dst = p1.dst(a2.getCenterX(),a2.getCenterY());
		P.free(p1);
		return dst;
	}
	
	public float getDistance2(HasCenter a1, HasCenter a2)
	{
		Vector2 p1 = P.getVector2().set(a1.getCenterX(),a1.getCenterY());
		final float dst2 = p1.dst2(a2.getCenterX(),a2.getCenterY());
		P.free(p1);
		return dst2;
	}
	
	public static float getAngle(HasCenter a1, HasCenter a2)
	{
		Vector2 p1 = P.getVector2().set(a1.getCenterX(),a1.getCenterY());
		final float angle = p1.angle(P.getVector2().set(a2.getCenterX(),a2.getCenterY()));
		P.free(p1);
		return angle;
	}
	
	@Override
	public void draw() {
		super.draw();
		
		if (radar.isVisible())
		{
			radar.rotateBy(-3);
		}

		
		if (!GameManager.renderStageDebug )
			return;
		ShapeRenderer shapeRenderer = new ShapeRenderer();

		for (Actor a : grp04enemy.getChildren()) {
			if (a instanceof Enemy) {
				// draw Enemy
				Enemy e = (Enemy) a;
				Vector2 center = e.getCenter();
				center = stageToScreenCoordinates(center);
				shapeRenderer.begin(ShapeType.Line);
				shapeRenderer.setColor(Color.RED);
				shapeRenderer.rect(center.x, Gdx.graphics.getHeight()- center.y, 5, 5);
				shapeRenderer.circle(center.x, Gdx.graphics.getHeight()- center.y, e.getHitRadius());
				shapeRenderer.end();
			}
		}
		for (Actor a : grp09projectiles.getChildren()) {
			if (a instanceof Rocket) {
				// draw Rocket
				Rocket e = (Rocket) a;
				Vector2 center = e.getCenter();
				center = stageToScreenCoordinates(center);
				shapeRenderer.begin(ShapeType.Line);
				shapeRenderer.setColor(Color.BLUE);
				shapeRenderer.rect(center.x, Gdx.graphics.getHeight()- center.y, 5, 5);
				shapeRenderer.circle(center.x, Gdx.graphics.getHeight()- center.y, 50);
				shapeRenderer.end();
			}
		}
		for (Actor a : grp03turret.getChildren()) {
			if (a instanceof Turret) {
				// draw Turret
				Turret e = (Turret) a;
				Vector2 center = e.getCenter();
				center = stageToScreenCoordinates(center);
				shapeRenderer.begin(ShapeType.Line);
				shapeRenderer.setColor(Color.GREEN);
				shapeRenderer.rect(center.x, Gdx.graphics.getHeight()- center.y, 5, 5);
				shapeRenderer.circle(center.x, Gdx.graphics.getHeight()- center.y, e.getRange());
				
				Vector2 direction = new Vector2(-1,1);
				direction.setLength(e.getRange());
				direction.setAngle(e.getAimDirection());
				
				float aoe; // AOE can only be rendered after config read
				try
				{
					aoe = e.getAOE();
				}
				catch (Throwable t)
				{
					aoe=10;
				}
				shapeRenderer.arc(center.x, Gdx.graphics.getHeight()- center.y, e.getRange(), e.getAimDirection()-(aoe/2f), aoe);
				
				shapeRenderer.setColor(Color.PINK);
				shapeRenderer.circle(center.x, Gdx.graphics.getHeight()- center.y, e.getCursorRadius());
				shapeRenderer.end();
			}
		}
		
		shapeRenderer.dispose();
	}

	//static public final ExpIn exp3In = new ExpIn(2, 3);

	public void freezeSurroundingEnemies(final HasCenter source, Enemy exception, float radius2) {
		//final float radius2 = radius*radius;
		
		doForAllEnemiesInRadius(source, exception, radius2, new PoorObjFloatConsumer<Enemy>() {
			@Override
			public void accept(Enemy enemy, float dst2) {
				// distance is fine, but enemy must also be in AOE
				if (source instanceof Freezer) {
					Freezer f = (Freezer) source;
					final float aimDirection = f.getAimDirection();
					final float angleToEnemy = enemy.getCenter().cpy().sub(source.getCenter()).angle();
					final boolean inAOE = Math.abs(aimDirection - angleToEnemy)<=f.getAOE()*0.8f; // the 0.8 opens the AOE a bit because hitradius is ignored
					if (inAOE)
						enemy.freezeTemporarily(1.1f);
				}
			}
		});
	}

	
	/**
	 * Adds radial (blast/explosion) damage to surrounding targets.
	 * @param center Source of explosion. Should be rocket at time of hit.
	 * @param exception Don't apply dmg to this one. Should be the primary hit target, that has received the main dmg already.
	 * The exception is not included in the blast, to use death prediction on the main target.
	 * @param damage max dmg that was applied to the main target
	 * @param blastRadius the radius around source to apply dmg to
	 */
	public void dealBlastDamage(HasCenter source, Enemy exception, int damage, float blastRadius) {
		/*
		 * Blast damage seemed unrealistic. Best solution is to reduce the primary damage
		 * before spreading it. That makes sense, because the primary hit of a rocket is
		 * much stronger than the shock wave around it.
		 * 
		 * Also here are all the variables that are constant for each blast.
		 */
		final float BLAST_DMG_SHOCK_WAVE_FACTOR=0.2f;
		final float fDamage = damage*BLAST_DMG_SHOCK_WAVE_FACTOR;
		final float blastRadius2 = blastRadius*blastRadius;
		
		doForAllEnemiesInRadius(source, exception, blastRadius2, new PoorObjFloatConsumer<Enemy>() {
			@Override
			public void accept(Enemy enemy, float dst2) {
				final float dmg = fDamage - Interpolation.exp5In.apply(0, fDamage, (dst2/blastRadius2));
				enemy.damage((int)dmg, DamageType.EXPLOSION);
			}
		});
		// TODO blast dmg seems to pull to the left, possible origin on 0,0 instead of center
	}

	/**
	 * execute a runnable for all enemies in a certain radius
	 */
	public void doForAllEnemiesInRadius(HasCenter source, Enemy exception, float radius2, PoorObjFloatConsumer<Enemy> toRun)
	{
		for (Iterator<Enemy> iterator = enemies.iterator(); iterator.hasNext();) {
			Enemy enemy = (Enemy) iterator.next();
			if (enemy == exception)
				continue;
			if (!enemy.isAlive())
				continue; // already killed by bullet or other blast dmg
			final float dst2 = getDistance2(source, enemy) + enemy.getHitRadius2();
			if (dst2 > radius2)
				continue; // to far away
			
			toRun.accept(enemy, dst2);
		}
	}

	public ArrayList<Enemy> getEnemiesToCascadeLightning(HasCenter source, Enemy exception, float radius, int max) {
		final ArrayList<Enemy> ret = new ArrayList<>(max);
		final float blastRadius2 = radius*radius;
		for (Iterator<Enemy> iterator = enemies.iterator(); iterator.hasNext();) {
			Enemy enemy = (Enemy) iterator.next();
			if (enemy == exception)
				continue;
			if (!enemy.isAlive())
				continue; // already killed by bullet or other blast dmg
			final float dst2 = getDistance2(source, enemy) + enemy.getHitRadius2();
			if (dst2 > blastRadius2)
				continue; // to far away
			ret.add(enemy);
			if (ret.size()>=max)
				break;
		}
		return ret;
	}

	public void addDropArea(Image validDropArea) {
		this.validDropArea= validDropArea;
		grp00background.addActor(validDropArea);
	}
	
	public boolean isInValidArea(Turret t)
	{
		final float x= t.getCenterX();
		final float y = t.getCenterY();
		byte c;
		boolean isOverOtherTurret = false;
		// ignore center, just use outer circle
		//c = getValidAreaColor(x,y);
		
		// check for overlap with another Turret (has to happen first to remove all cursors)
		for (Turret turret : turrets) {
			if (turret ==t)
				continue;
			
			// all other turrets should be unselected anyway
			turret.hideCursor();
			
			final float dst_2 = getDistance2(t, turret);
			final float r1_2 = t.getCursorRadius() * t.getCursorRadius();
			final float r2_2 = turret.getCursorRadius() * turret.getCursorRadius();
			if (r1_2 + r2_2+r1_2 + r2_2 > dst_2) // TODO check why x2 . Maybe cursor is drawn in 200% size
			{
				// mark colliding torrent temporarily red
				turret.showRedCursor();
				// cannot return here yet: iterate all to remove all cursors
				isOverOtherTurret = true;
			}
		}
		
		if (isOverOtherTurret)
			return false;
		
		// check validDropArea on map
		getCircleAroundPoint(x, y, t.getCursorRadius());
		for (Vector2 v : circleAroundPoint) {
			c = getValidAreaColor(v.x,v.y);
			//Gdx.app.debug("isInValidArea ", ""+c);
			if (c==0)
				return false;
		}
		
		return true;
	}

	Pixmap pixmap = null;
	private byte getValidAreaColor(float x, float y) {
		if (pixmap == null)
		{
			TextureRegionDrawable o = (TextureRegionDrawable)validDropArea.getDrawable();
			final Texture texture = o.getRegion().getTexture();
			if (!texture.getTextureData().isPrepared()) {
	            texture.getTextureData().prepare();
	        }
			pixmap = texture.getTextureData().consumePixmap();
		}
		int textureLocalX =  (int)x;
        int textureLocalY =  GameScreen.TEX_SIZE - (int)y;
        
        // the png minimization saves palette space, by adding a color to the transparent
        // pixels. nonetheless, they have last 8 bits = alpha = 00000000
        final int pixel32 = pixmap.getPixel(textureLocalX, textureLocalY);
		//Gdx.app.debug("getValidAreaColor pixel value ", ""+(byte)pixel32);
		return (byte)pixel32;
	}
	
	ArrayList<Vector2> circleAroundPoint = new ArrayList<>();
	private void getCircleAroundPoint (float x, float y, float radius, int segments) {
		for (Vector2 v : circleAroundPoint) {
			P.free(v);
		}
		circleAroundPoint.clear();
		if (segments <= 0) throw new IllegalArgumentException("segments must be > 0.");
		float angle = 2 * MathUtils.PI / segments;
		float cos = MathUtils.cos(angle);
		float sin = MathUtils.sin(angle);
		float cx = radius, cy = 0;
		for (int i = 0; i < segments; i++) {
			circleAroundPoint.add(P.getVector2(x + cx, y + cy));
			float temp = cx;
			cx = cos * cx - sin * cy;
			cy = sin * temp + cos * cy;
			
			// next point will be added in next loop-round or in final instruction
			//circleAroundPoint.add(P.getVector2(x + cx, y + cy));
		}
		// Ensure the last segment is identical to the first.
		circleAroundPoint.add(P.getVector2(x + cx, y + cy));

	}
	
	private void getCircleAroundPoint (float x, float y, float radius) {
		getCircleAroundPoint(x, y, radius, Math.max(1, (int)(6 * (float)Math.cbrt(radius))));
	}
	
	@Override
	public void dispose() {
		if (pixmap != null)
			pixmap.dispose();
		super.dispose();
	}



}
