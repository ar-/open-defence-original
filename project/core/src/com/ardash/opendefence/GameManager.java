/*******************************************************************************
 * Copyright (C) 2017 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence;

import java.util.HashMap;

import com.ardash.opendefence.beans.ETD;
import com.ardash.opendefence.beans.Map;
import com.ardash.opendefence.beans.SpawnDescriptor;
import com.ardash.opendefence.beans.Wave;
import com.ardash.opendefence.beans.Way;
import com.ardash.opendefence.beans.WayList;
import com.ardash.opendefence.beans.WayPoint;
import com.ardash.opendefence.common.ValueChangeListener;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.ObjectMap.Entry;


public class GameManager {
	
	//public static final boolean renderStageDebug=false;
	public static final boolean renderStageDebug=true;
	public static final boolean renderGuiDebug=true;
	public static final boolean renderFPSIndicator = true;
	public static final boolean runSelfTest = true;
	
	private final OpenDefenceGame game;
	private final ArrayMap<String,ETD> enemyTypeDescriptors;
	private int difficultily;
	private int money;
	private long points;
	private int health;
	private int wave;
	private Map currentMap = null;
	
	private ValueChangeListener<Integer> moneyChangedListener = null;
	private ValueChangeListener<Long> pointsChangedListener = null;
	private ValueChangeListener<Integer> healthChangedListener = null;
	private ValueChangeListener<Integer> waveChangedListener = null;
	
	public GameManager(OpenDefenceGame game) {
		super();
		this.game = game;
		enemyTypeDescriptors = new ArrayMap<>();
		reset();
	}

	public void reset()
	{
		System.gc();
		difficultily = 0;
		money = 0;
		health = 20;
		wave = 0;
		currentMap = null;
		
		if (moneyChangedListener!=null)
			moneyChangedListener.onValueChange(money);
		if (pointsChangedListener!=null)
			pointsChangedListener.onValueChange(points);
		if (healthChangedListener!=null)
			healthChangedListener.onValueChange(health);
		if (waveChangedListener!=null)
			waveChangedListener.onValueChange(wave);
	}
			
	private void resetWave()
	{
		System.gc();
	}
	
	public void addMoney(int amount)
	{
		money+=amount;
		if (moneyChangedListener!=null)
			moneyChangedListener.onValueChange(money);
	}

	public void addPoints(long p) {
		points+=p;
		if (pointsChangedListener!=null)
			pointsChangedListener.onValueChange(points);
	}
	
	public void reduceHealthByOne()
	{
		health--;
		if (healthChangedListener!=null)
			healthChangedListener.onValueChange(health);
	}
			
	public void startNextWave()
	{
		resetWave();
		wave++;
		if (waveChangedListener!=null)
			waveChangedListener.onValueChange(wave);
	}

	public int getDifficultily() {
		return difficultily;
	}

	public void setDifficultily(int difficultily) {
		this.difficultily = difficultily;
	}

	public void setMoneyChangedListener(ValueChangeListener<Integer> moneyChangedListener) {
		this.moneyChangedListener = moneyChangedListener;
	}

	public void setPointsChangedListener(ValueChangeListener<Long> pointsChangedListener) {
		this.pointsChangedListener = pointsChangedListener;
	}

	public void setHealthChangedListener(ValueChangeListener<Integer> healthChangedListener) {
		this.healthChangedListener = healthChangedListener;
	}

	public void setWaveChangedListener(ValueChangeListener<Integer> waveChangedListener) {
		this.waveChangedListener = waveChangedListener;
	}

	/**
	 * reads a map file without loading it and without making it the current map
	 * @param mapname
	 * @return the map bean
	 */
	public Map peekMap(String mapname) {
		reset();
		FileHandle fh = Gdx.files.internal("conf/map/"+mapname+".json");
		//FileHandle fh = Gdx.files.local("../android/assets/conf/map/map_01.json");
		Json json = new Json();
		json.setElementType(Map.class, "wayList", Way.class);
		json.setElementType(Map.class, "waveList", Wave.class);
		return json.fromJson(Map.class, fh);
	}

	public void loadMap(String mapname) {
		currentMap  = peekMap(mapname);
	}

//	public Map getCurrentMap ()
//	{
//		return currentMap;
//	}
	
	public String getCurrenttextureName ()
	{
		return currentMap.textureName;
	}
	
	public WayList getListOfWays()
	{
		return currentMap.wayList;
	}
	
	public Wave getWave()
	{
		return currentMap.waveList.get(wave);
	}

	public int getAmountOfWaves()
	{
		return currentMap.waveList.size();
	}
	
	public boolean isInLastWave()
	{
		return wave==getAmountOfWaves()-1;
	}
	
	public void loadEnemyTypeDescriptors() {
		FileHandle dirHandle;
		if (Gdx.app.getType() == ApplicationType.Android) {
		   dirHandle = Gdx.files.internal("conf/enemy");
		} else {
			  dirHandle = Gdx.files.internal("assets/conf/enemy");
			  //dirHandle = Gdx.files.internal("./bin/conf/enemy");
		}
		  
		Json json = new Json();
		for (FileHandle fh: dirHandle.list()) {
			ETD etd = json.fromJson(ETD.class, fh);
			enemyTypeDescriptors.put(etd.ID, etd);
		}
		
		if (enemyTypeDescriptors.size == 0)
			throw new RuntimeException("no EnemyTypeDescriptors loaded. probably looking in wrong directory");
	}
	
	public ETD getEnemyTypeDescriptor(String etid)
	{
		final ETD etd = enemyTypeDescriptors.get(etid);
		if (etd == null)
			throw new RuntimeException("ETD not found: "+ etid);
		return etd;
	}


	void runSelfTests() {
		if (! runSelfTest)
			return;
		
		HashMap<ETD, String> etdMap = new HashMap<>();

		runSelfTest(etdMap, "map_r01");
		runSelfTest(etdMap, "map_r02");
		runSelfTest(etdMap, "map_r03");
		
		validateEnemyTypeDescriptors();
		
	}

	private void validateEnemyTypeDescriptors() {
		// there is already a check to verify all ETDs in waves, this one checks all available files
		if (enemyTypeDescriptors == null)
		{
			loadEnemyTypeDescriptors();
		}
		
		for (Entry<String, ETD> entry : enemyTypeDescriptors) {
			String name = entry.key;
			ETD etd = entry.value;
			assert name.equals(etd.ID);
			if (name.equals("soldier"))
			{
				assert etd.isHuman;
			}
			else
			{
				assert ! etd.isHuman;
			}
			assert etd.hp >=100;
			assert etd.speed <=100;
			assert etd.speed >=50;
			assert etd.value >=1;
			assert etd.value <=5;

			// check if textures exist
//			final TextureRegion tr = A.getTextureRegion(etd.textureName, true); // should not cause an exception
//			assert tr.getRegionHeight()>1;
//			assert tr.getRegionWidth()>1;
//			final TextureRegion tr_ice = A.getTextureRegion(etd.textureName+"_ice", true); // should not cause an exception
//			assert tr_ice.getRegionHeight()>1;
//			assert tr_ice.getRegionWidth()>1;
//			assert tr_ice.getRegionHeight() == tr.getRegionHeight();
//			assert tr_ice.getRegionWidth() == tr.getRegionWidth();
		}
		
	}

	private void runSelfTest(HashMap<ETD, String> etdMap, String mapname) {
		Gdx.app.log("selftext", "loading "+mapname);
		loadMap(mapname);
		assert currentMap != null;
		assert currentMap.startMoney >=20;
		assert currentMap.textureName != null;
		assert currentMap.label != null;
		assert ! currentMap.label.equals("");
		final FileHandle fh1 = Gdx.files.internal("assets/maps/"+mapname+".jpg");
		assert ! fh1.isDirectory();
		assert fh1.exists();
		assert fh1.length() < 1800000L;
		assert fh1.length() > 100000L;
		final FileHandle fh2 = Gdx.files.internal("assets/maps/"+mapname+"_a.png");
		assert ! fh2.isDirectory();
		assert fh2.exists();
		assert fh2.length() < 350000L;
		assert fh2.length() > 100000L;
		final FileHandle fh3 = Gdx.files.internal("assets/maps/"+mapname+"_t.png");
		if (fh3.exists())
		{
			assert ! fh3.isDirectory();
			assert fh3.exists();
			assert fh3.length() < 1800000L;
			assert fh3.length() > 100000L;
		}
		assert currentMap.waveList != null;
		assert currentMap.wayList != null;
		assert currentMap.waveList.size() > 0;
		assert currentMap.wayList.size() > 0;
		for (Way w : currentMap.wayList) {
			assert w.wayPoints.size() >= 2;
			for (WayPoint wp : w.wayPoints) {
				assert wp.v >=0;
			}
		}
		for (Wave w : currentMap.waveList) {
			for (SpawnDescriptor sd : w) {
				assert sd.amount >0;
				assert sd.delay >0;
				assert sd.start >= 0;
				assert sd.wayIndex < currentMap.wayList.size(); // check if way is available
				final ETD etd = getEnemyTypeDescriptor(sd.etid); // should not cause an exception
				
				assert etd.hp > 1;
				assert etd.speed > 0;
				assert etd.value > 0;
				assert etd.hp > 1;
				assert etd.ID != null && !etd.ID.equals("");
				assert etd.textureName != null && !etd.textureName.equals("");
				
				// check if IDs are unique over all files
				if (etdMap.containsKey(etd))
				{
					assert etdMap.get(etd).equals(etd.ID);
				}
				else
				{
					etdMap.put(etd, etd.ID);
				}
				
				// check if textures exist
//				final TextureRegion tr = A.getTextureRegion(etd.textureName, true); // should not cause an exception
//				assert tr.getRegionHeight()>1;
//				assert tr.getRegionWidth()>1;
			}
		}
	}



}
