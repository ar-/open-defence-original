/*******************************************************************************
 * Copyright (C) 2017 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence;

import com.ardash.opendefence.screens.LoadingScreen;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pools;

public class OpenDefenceGame extends Game {
//	public final AssetManager am ;
	public final GameManager gm;
//	static public Skin skin;
	
	public OpenDefenceGame(){
//		am = new AssetManager();
//		Texture.setAssetManager(am);
		gm = new GameManager(this);
		Pools.get(Vector2.class, 200); // make the vector pools a bit larger
	}

	@Override
	public void create () {
		addAssets();
//		A.getInstance(am).load(); // init A-singleton
		gm.loadEnemyTypeDescriptors();
		gm.runSelfTests();
		//skin = new Skin(Gdx.files.internal("skin/glassy-ui.json"));
		this.setScreen(new LoadingScreen(this));

	}

	private void addAssets() {
//		am.load(Asset.skin.d);
		//am.finishLoading();
	}
	
	@Override
	public void render () {
		super.render();
//		am.update();
	}

	@Override
	public void dispose () {
//		am.dispose();
		super.dispose();
		if (screen !=null)
			screen.dispose();
		A.dispose();
	}
	
	@Override
	public void pause() {
		super.pause();
//		SoundPlayer.stopSound(A.get(A.soundIceSpray));
	}
	
	@Override
	public void resume() {
//		am.finishLoading();
		super.resume();
	}

}
