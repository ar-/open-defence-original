package com.ardash.opendefence;

import com.ardash.opendefence.effects.LightningBolt;
import com.ardash.opendefence.effects.Line;
import com.ardash.opendefence.effects.SmokeTrail;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pools;

/**
 * A short convienience thing for pools
 * @author Andreas Redmer
 */
public class P {
	
	static {
		Pools.get(Line.class, 1000);
		Pools.get(SmokeTrail.class, 8000);
	}
	
	public static void free(Object... o) {
		for (Object object : o) {
			Pools.free(object);
		}
	}

	public static Vector2 getVector2()
	{
		final Vector2 o = Pools.get(Vector2.class,200).obtain();
		o.set(0,0);
		return o;
	}

	public static Vector2 getVector2(float x, float y) {
		return getVector2().set(x, y);
	}

	public static Vector2 getVector2(Vector2 v) {
		return getVector2().set(v.x, v.y);
	}

	public static Line getLine(Vector2 a, Vector2 b, float thickness) {
		final Line line = Pools.get(Line.class, 1000).obtain();
		line.init(a, b, thickness);
		return line;
	}

	public static Color getColor() {
		return Pools.get(Color.class).obtain().set(0,0,0,0);
	}
	public static Color getColor(Color c) {
		return Pools.get(Color.class).obtain().set(c);
	}

	public static LightningBolt getLightningBolt(Vector2 source, Vector2 dest) {
		final LightningBolt bold = Pools.get(LightningBolt.class).obtain();
		bold.init(source, dest);
		return bold;
	}

}
