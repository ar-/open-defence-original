/*******************************************************************************
 * Copyright (C) 2017,2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.actions;

import com.ardash.opendefence.P;
import com.ardash.opendefence.common.HasCenter;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;

public class AimTo extends Action {
	
	/**
	 * The enemy to aim at.
	 */
	private HasCenter enemy;
	
	/**
	 * Rotational speed in degrees per second.
	 */
	private float rotationSpeed;
	
	/**
	 * remaining angle to Enemy, after aiming
	 */
	private float angleToEnemy=Float.MAX_VALUE;

	/**
	 * Rotation has has actually been applied to the actor after act().
	 * This is store here, so it can be used in class FlyTo.
	 */
	protected float rotationAppliedToActor;
	
	

	public AimTo(HasCenter enemy, float rotationSpeed) {
		super();
		this.enemy = enemy;
		this.rotationSpeed = rotationSpeed;
	}
	
	public void setEnemy(HasCenter enemy) {
		this.enemy = enemy;
	}

	public void setRotationSpeed(float rotationSpeed) {
		this.rotationSpeed = rotationSpeed;
	}

	@Override
	public boolean act(float delta) {
		Vector2 enemyPos = enemy.getCenter();
		
		// for testing the next line aims at the mouse pos
		//enemyPos=target.getStage().screenToStageCoordinates(P.getVector2(Gdx.input.getX(),Gdx.input.getY()));		

		Vector2 myPos =P.getVector2();
		final Vector2 originVector = P.getVector2().set(target.getOriginX(),target.getOriginY());
		myPos.add(target.localToStageCoordinates(originVector));

		// calculate in double. float is to inaccurate and causes artifacts
		double targetAngle = Math.atan2(enemyPos.y - myPos.y, enemyPos.x - myPos.x );
		targetAngle = targetAngle * (180/Math.PI);
		float targetAngleF = (float)targetAngle+180;
		
		float from = target.getRotation();
		float to = targetAngleF;
		
		//if (Math.abs(from-to) > 180)
		while (Math.abs(from-to) > 180)
		{
			if (to>from)
			{
				to = to-360;
			}
			else
			{
				from = from-360;
			}
		}
		
		// determine maximum rotation angle for this tick
		final float maxDegreesPerSec = rotationSpeed;
		final float maxDegreesForThisTick = maxDegreesPerSec * delta;
		
		float rotAngle = MathUtils.clamp((to - from), -maxDegreesForThisTick, maxDegreesForThisTick);
		rotationAppliedToActor = from + rotAngle;
		rotationAppliedToActor %= 360; // don't grow the numerics infinitely
		target.setRotation(rotationAppliedToActor);
		
		// since all the calculations are already done here:
		// publish the remaining angle to the target
		// so it can be used for shooting if the enemy is in AOE
		angleToEnemy = target.getRotation()-to;
		//Gdx.app.debug("AimTo", "from: "+from+" to: "+to + " applied: " + rotationAppliedToActor);
		
		P.free(myPos);
		P.free(originVector);
		P.free(enemyPos);

		// this action is never over, it will be removed by the turret, if target get out of range or dies
		return false;
	}

	public float getAngleToEnemy() {
		return angleToEnemy;
	}
	
	

}
