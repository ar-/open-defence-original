/*******************************************************************************
 * Copyright (C) 2017 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.actions;

import com.ardash.opendefence.P;
import com.ardash.opendefence.common.HasCenter;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;

public class FlyTo extends AimTo {
	
	/**
	 * The speed (in units per second) of the fly-movement of the projectile.
	 */
	private final float speed;
	
	/**
	 * reuse this vector to reduce GC load
	 */
	private final Vector2 currentTickMovement = P.getVector2();

	/**
	 * measure how long the action is already running, to do interpolation in the beginning
	 */
	private float lifetime;

	public FlyTo(HasCenter enemy, float rotationSpeed, float movementSpeed) {
		super(enemy, rotationSpeed);
		speed = movementSpeed;
		lifetime = 0;
	}
	
	@Override
	public boolean act(float delta) {
		super.act(delta);
		lifetime += delta;
		currentTickMovement.set(1, 1);
		currentTickMovement.setAngle(rotationAppliedToActor-180);
		
		if (lifetime<1.0f)
		{
			final float progress = Math.min(1f, lifetime/0.5f);
			final float interpolatedSpeed = Interpolation.pow3Out.apply(0, speed, progress);
			currentTickMovement.setLength(delta * interpolatedSpeed);
		}
		else
			currentTickMovement.setLength(delta * speed);
		
		
		target.moveBy(currentTickMovement.x, currentTickMovement.y);
		return false;
	}

}
