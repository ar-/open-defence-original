/*******************************************************************************
 * Copyright (C) 2017 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.actions;

import com.ardash.opendefence.A;
import com.ardash.opendefence.A.SoundAsset;
import com.ardash.opendefence.P;
import com.ardash.opendefence.common.DamageType;
import com.ardash.opendefence.common.Enemy;
import com.ardash.opendefence.common.HasCenter;
import com.ardash.opendefence.effects.BulletHit;
import com.ardash.opendefence.helpers.SoundPlayer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.utils.Pools;

/**
 * Plays sound, animates the turret (force feedback, barrel rotate), and deals damage.
 *
 */
public class GunShotAction extends Action {
	
	private static final float FORCE_FEEDBACK_ANIMATION_TIME = 0.04f; 
	private static final float FORCE_FEEDBACK_ANIMATION_STRENGTH = 5f; 
	
	private final Enemy enemy;
	private final int damage;

	public GunShotAction(Enemy e, int damage) {
		enemy = e;
		this.damage = damage;
	}

	@Override
	public boolean act(float delta) {
		//Gdx.app.debug("GunShootAction", "act");
		
		// just in case it got killed since last acting frame by blast dmg
		if (enemy==null || enemy.getStage()==null)
			return true;
		
		// play sound
		SoundPlayer.playSound(A.getSound(SoundAsset.GATLING1));

		// show bullet hit on enemies
		// must be done before inflicting damage, because  otherwise enemy is not on stage any more
		BulletHit bh = Pools.get(BulletHit.class).obtain();
		bh.init(enemy, ((HasCenter)getTarget().getParent()).getCenterX(), ((HasCenter)getTarget().getParent()).getCenterY());

		//cause damage
		enemy.damage(damage, DamageType.BULLET);		
		
		// get force feedback direction
		Vector2 force = P.getVector2(1,1);
		force.setAngle(getTarget().getRotation());
		force.setLength(FORCE_FEEDBACK_ANIMATION_STRENGTH);
		
		// play animation of gun force feedback
		MoveByAction move1 = Actions.moveBy(force.x, force.y, FORCE_FEEDBACK_ANIMATION_TIME);
		MoveToAction move2 = Actions.moveTo(getTarget().getX(), getTarget().getY(), FORCE_FEEDBACK_ANIMATION_TIME);
		getTarget().addAction(Actions.sequence(move1, move2));
		P.free(force);
		return true;
	}

}
