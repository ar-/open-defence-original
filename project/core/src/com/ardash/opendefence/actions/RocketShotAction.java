/*******************************************************************************
 * Copyright (C) 2017, 2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.actions;

import com.ardash.opendefence.P;
import com.ardash.opendefence.common.Enemy;
import com.ardash.opendefence.turrets.Rocket;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;

/**
 * Plays sound, animates the turret (force feedback, barrel rotate), and deals damage.
 * @author Andreas Redmer
 *
 */
public class RocketShotAction extends Action {
	
	private static final float FORCE_FEEDBACK_ANIMATION_TIME = 0.04f; 
	private static final float FORCE_FEEDBACK_ANIMATION_STRENGTH = 2f; 
	
	private final Enemy enemy;
	private final int damage;
	private final float blastRadius;

	public RocketShotAction(Enemy e, int damage, float blastRadius) {
		enemy = e;
		this.damage = damage;
		this.blastRadius = blastRadius;
	}

	@Override
	public boolean act(float delta) {
		//Gdx.app.debug("RocketShotAction", "act");
		// rocket plays sound by itself

		// get force feedback direction
		Vector2 force = P.getVector2(1,1);
		force.setAngle(getTarget().getRotation());
		force.setLength(FORCE_FEEDBACK_ANIMATION_STRENGTH);
		
		// play animation of gun force feedback
		MoveByAction move1 = Actions.moveBy(force.x, force.y, FORCE_FEEDBACK_ANIMATION_TIME);
		MoveToAction move2 = Actions.moveTo(getTarget().getX(), getTarget().getY(), FORCE_FEEDBACK_ANIMATION_TIME);
		getTarget().addAction(Actions.sequence(move1, move2));
		
		//spawn new rocket (rocket actor has always target, fly-action and short life time)
		Vector2 spawnPos = P.getVector2(target.getOriginX(), target.getOriginY());
		spawnPos = target.localToStageCoordinates(spawnPos);
		final Pool<Rocket> rocketPool = Pools.get(Rocket.class);
		final Rocket rocket = rocketPool.obtain();
		rocket.init(spawnPos.x, spawnPos.y, target, enemy, damage, blastRadius);
		P.free(force);
		P.free(spawnPos);
		return true;
	}

}
