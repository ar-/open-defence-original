/*******************************************************************************
 * Copyright (C) 2017, 2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.actions;

import java.util.ArrayList;

import com.ardash.opendefence.DefenceStage;
import com.ardash.opendefence.P;
import com.ardash.opendefence.common.DamageType;
import com.ardash.opendefence.common.Enemy;
import com.ardash.opendefence.common.HasCenter;
import com.ardash.opendefence.effects.LightningBolt;
import com.ardash.opendefence.turrets.Turret;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;

/**
 * Plays sound, causes lightnings (normal, cascaded and splits), and deals damage.
 *
 */
public class TeslaShotAction extends Action {
	
	private final Enemy enemy;
	private final int damage;

	public TeslaShotAction(Enemy e, int damage) {
		enemy = e;
		this.damage = damage;
	}

	@Override
	public boolean act(float delta) {
		//Gdx.app.debug("TeslaShotAction", "act");
		
		// just in case it got killed since last acting frame by blast dmg
		if (enemy==null || getStage()==null)
			return true;
		
		// TODO play sound

		// prepare
		final Vector2 centerOfEnemy = enemy.getCenter();

		// show lightning bolt
		// must be done before inflicting damage, because  otherwise enemy is not on stage any more
		LightningBolt lightningBolt = P.getLightningBolt(((HasCenter)getTarget()).getCenter(), centerOfEnemy);
		getStage().grp09projectiles.addActor(lightningBolt);
		
		// on higher levels shoot at additional enemies cascaded
		if (getTarget() instanceof Turret) {
			Turret primatryShooter = (Turret) getTarget();
			final int level = primatryShooter.getLevel();
			if (level>0)
			{
				// show the primary lightning split on higher levels
				final Vector2 splitPoint = lightningBolt.getSplitPoint();
				centerOfEnemy.add(MathUtils.random(-15, 15), MathUtils.random(-15, 15));
				lightningBolt = P.getLightningBolt(splitPoint, centerOfEnemy);
				getStage().grp09projectiles.addActor(lightningBolt);

				// cascade
				final ArrayList<Enemy> nearByEnemies = getStage().getEnemiesToCascadeLightning(enemy, enemy, 100f, level);
				for (Enemy nearEnemy : nearByEnemies) {
					enemy.addAction(new TeslaShotAction(nearEnemy,damage/2));
				}
				
			}
		}

		//cause damage (after cascading because first enemy must still be alive to forward damage
		enemy.damage(damage, DamageType.ELECTRO);
		
		return true;
	}

	private DefenceStage getStage() {
		return enemy.getStage();
	}

}
