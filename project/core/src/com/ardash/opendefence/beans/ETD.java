/*******************************************************************************
 * Copyright (C) 2017, 2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.beans;

/**
 * EnemyTypeDescriptor
 * @author Andreas Redmer
 *
 */
public class ETD {
	public String ID;
	public String textureName;
	public int hp;
	/**
	 * money received when destroyed
	 */
	public int value;
	public boolean isHuman;
	public float speed;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ID == null) ? 0 : ID.hashCode());
		result = prime * result + hp;
		result = prime * result + (isHuman ? 1231 : 1237);
		result = prime * result + Float.floatToIntBits(speed);
		result = prime * result
				+ ((textureName == null) ? 0 : textureName.hashCode());
		result = prime * result + value;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ETD other = (ETD) obj;
		if (ID == null) {
			if (other.ID != null)
				return false;
		} else if (!ID.equals(other.ID))
			return false;
		if (hp != other.hp)
			return false;
		if (isHuman != other.isHuman)
			return false;
		if (Float.floatToIntBits(speed) != Float.floatToIntBits(other.speed))
			return false;
		if (textureName == null) {
			if (other.textureName != null)
				return false;
		} else if (!textureName.equals(other.textureName))
			return false;
		if (value != other.value)
			return false;
		return true;
	}

	
}
