/*******************************************************************************
 * Copyright (C) 2017 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.beans;


/**
 * TurretTypeDescriptor
 * @author Andreas Redmer
 *
 */
public class TTD {
	public String baseTexture;
	
	/**
	 * range for radar display and shooting
	 */
	public float range;
	
	/**
	 * Angle of effect. If a target is in this angle, it will fire.
	 * This is an accepted tolerance to aim to the target.
	 * Otherwise aiming would be too hard.
	 */
	public float AOE;
	
	/**
	 * fire rate in shots per second ( can be <0 ) 
	 */
	public float rate;
	
	/**
	 * cost to gain this level of turret.
	 * for level 0 this is the initial cost
	 */
	public int cost;
	
	/**
	 * damage
	 */
	public int dmg;
	
	/**
	 * blast radius (ignored for none explosives)
	 */
	public float blastradius;
	
	/**
	 * rotation speed in degree per second
	 */
	public float rotSpeed;
	
	public void verify()
	{
		if (range<=10f || range>1024f )
			throw new RuntimeException("invalid range " + toString());
		
		if ((AOE<1f || AOE>90f) && AOE<360f) 
			throw new RuntimeException("invalid AOE " + toString());
		
		if (rate<0.1f || rate>30f) // fire rate can't be higher than FPS
			throw new RuntimeException("invalid rate " + toString());
		
		if (cost<20 || cost>1600)
			throw new RuntimeException("invalid cost " + toString());
		
		if (dmg<1 || dmg>24000)
			throw new RuntimeException("invalid dmg " + toString());
		
		if ((blastradius<50 && blastradius>0.1f) || blastradius>300)
			throw new RuntimeException("invalid blastradius " + toString());
		
		if (rotSpeed<45f || rotSpeed>360f)
			throw new RuntimeException("invalid rotSpeed " + toString());
		
	}

	@Override
	public String toString() {
		return "TTD [baseTexture=" + baseTexture + ", range=" + range
				+ ", AOE=" + AOE + ", rate=" + rate + ", cost=" + cost
				+ ", dmg=" + dmg + ", blastradius=" + blastradius
				+ ", rotSpeed=" + rotSpeed + "]";
	}

	
	
}
