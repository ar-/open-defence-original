/*******************************************************************************
 * Copyright (C) 2017 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.beans;

import java.util.ArrayList;
import java.util.Iterator;

public class Wave implements Iterable<SpawnDescriptor>{
	ArrayList<SpawnDescriptor> items = new ArrayList<>();

	public void add(SpawnDescriptor sd1) {
		items.add(sd1);
	}
	
	@Override
	public Iterator<SpawnDescriptor> iterator()
	{
		 return items.iterator();
	}
}
