/*******************************************************************************
 * Copyright (C) 2017 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.beans;

public class WayPoint {
	public float x;
	public float y;
	/**
	 * variance
	 */
	public float v;
	public WayPoint() {}
	public WayPoint(WayPoint wp) 
	{
		x = wp.x;
		y = wp.y;
		v = wp.v;
	}
	
	public WayPoint(float x, float y, float v, String comment) {
		super();
		this.x = x;
		this.y = y;
		this.v = v;
	}

	
}
