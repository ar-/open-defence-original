/*******************************************************************************
 * Copyright (C) 2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.common;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public abstract class AnimatedImage extends SelfDestructableImage
{
    protected Animation<AtlasRegion> animation = null;
    protected boolean looping = true;
    private float stateTime = 0;

    public AnimatedImage(Animation<AtlasRegion> animation) {
        super(animation.getKeyFrame(0));
        this.animation = animation;
    }

//    public AnimatedImage(Animation<AtlasRegion> animation) {
//        super(animation.getKeyFrame(0));
//        this.animation = animation;
//    }

    @Override
    public void act(float delta)
    {
        ((TextureRegionDrawable)getDrawable()).setRegion(animation.getKeyFrame(stateTime+=delta, looping));
        super.act(delta);
    }

    @Override
    public void reset() {
    	stateTime = 0;
    	looping = true;
	}
    
    /**
     * Set the time TTL in super class and the time per frame according to the amount of frames in this animation.
     */
	@Override
	protected void setTimeToLive(float d) {
		super.setTimeToLive(d);
		final Object[] allFrames = animation.getKeyFrames();
		final int frameCount = allFrames.length;
		final float timePerFrame = d/(float)frameCount;
		animation.setFrameDuration(timePerFrame);
	}
}
