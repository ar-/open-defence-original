package com.ardash.opendefence.common;


public enum DamageType {
	BULLET,
	ELECTRO,
	EXPLOSION,
	FIRE,
	NONE;

	/**
	 * @return TRUE if the damage-type would cause blood (like bullet or explosion), 
	 * ortherwise FALSE (like fire or electro)
	 */
	public boolean isBloody()
	{
		return (this.equals(BULLET) || this.equals(EXPLOSION));
	}
}
