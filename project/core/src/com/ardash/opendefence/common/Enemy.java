/*******************************************************************************
 * Copyright (C) 2017,2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.common;

import com.ardash.opendefence.A;
import com.ardash.opendefence.DefenceStage;
import com.ardash.opendefence.P;
import com.ardash.opendefence.beans.Way;
import com.ardash.opendefence.beans.WayPoint;
import com.ardash.opendefence.effects.BloodMark;
import com.ardash.opendefence.effects.BurnMark;
import com.ardash.opendefence.effects.Explosion;
import com.ardash.opendefence.effects.Mark;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.CatmullRomSpline;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pools;

/**
 * Speed: from start of map to end of map: 
 * Soldier: 63 seconds
 * jeep: 36 seconds
 * truck: 63 sec
 * tank: 63 sec
 * 
 * speed: 100 -> 28 sec
 * speed 50 -> 57 sec
 * @author Andreas Redmer
 *
 */
public class Enemy extends Group implements HasCenter{
	
	private static final float VISIBLE_TIME_OF_GREEN_HEALTHBAR = 2;
	
	/**
	 * health points
	 */
	protected int HP;
	
	/**
	 * Announced damage, is the damage that is soon to some in.
	 * This is to predict future death and not to shoot an this enemy
	 * anymore. Announced damage has to be added AND REMOVED
	 * from this value by the damage dealer.
	 * 
	 * Note: blast damage can not be predicted, because of the variance in enemy-speed.
	 */
	protected int announcedDamage=0;
	
	/**
	 * Records the damage type that was received last. This is only important for
	 * human enemies, because they look different depending on the last received damage.
	 * They can either leave a burnmark (fire, electro) or a bloodmark (rocket, bullet)
	 */
	private DamageType lastDamageType = DamageType.NONE;
	
	private boolean alive = true;
	final private Image image;
	private Image iceImage; // overlay image for iced actor
	final private int maxHP;
    float speed;
	private float lifeTime = 0;
	private float lastShotTimestamp = 0;
	final private Way way;
	final private int value;
	final private boolean isHuman;
	private boolean healthBarVisible=false;
	private boolean isIced=false;
	private float timestampToUnfreeze;
	SequenceAction iceActionSequence;
	private Color healthBarColor = Color.BLACK;
	
	// path members for catmull algorithm
	// int k = 100; //increase k for more fidelity to the spline
    float progressOnPath = 0;
    Vector2[] catmullDataSet; 
    CatmullRomSpline<Vector2> catmull;
    Vector2 catmullOut = P.getVector2();
	
	// some attributes stored to make it faster
	
	/**
	 * radius to receive hits
	 */
	private final float hitRadius;
	
	/**
	 * squared @see hitradius
	 */
	private final float hitRadius2;

	/**
	 * Flag will be set to true by the die() method.
	 * Then this actor dies in the next act().
	 */
	private boolean ismarkedForDeath =false;

	public Enemy (Array<AtlasRegion> t, int initialHP, float speed, Way way, int value, boolean isHuman)
	{
		maxHP = initialHP;
		HP = initialHP;
		this.speed = speed;
		this.way = way;
		this.value = value;
		this.isHuman = isHuman;
		
		if (t.size==1)
			// not animated
			image = new Image(t.get(0));
		else
		{
			// animated
			image = new AnimatedImage(getAnimation(t)) {
				@Override
				protected void free() {
					// not pooled
				}
			};
		}
			
		/*
		 * all enemies must be designed twice as big, they are  scaled  down here
		 * that way they still look good when zoomed in, because max zoom is 0.5
		 */
		image.setScale(0.5f);
		
		image.setOrigin(image.getWidth()/2, image.getHeight()/2);
		addActor(image);
		initPath();
		initFrozenImage(t);

		// note divide by 4 because we need avg of half of height/width
		// then divide again by 2 because of the scale 0.5
		hitRadius  = ((image.getWidth() + image.getHeight()) /4f) /2f;
		hitRadius2 = hitRadius * hitRadius;
		
		setIced(false);
	}

	/**
	 * The frozen buff is shown as an overlay on top of the image.
	 * If it is available, it will be initialized here.
	 * @param t 
	 */
	private void initFrozenImage(Array<AtlasRegion> t) {
		final String iceName = t.get(0).name+"_ice";
		final TextureRegion iceTexture = A.getTextureRegion(iceName, false);
		iceImage = new Image(iceTexture);
		iceImage.setScale(0.5f);
		iceImage.setOrigin(iceImage.getWidth()/2, iceImage.getHeight()/2);
		addActor(iceImage);
		iceImage.setVisible(false);
	}

	/**
	 * to be used in constructor
	 * @return
	 */
	private static Animation<AtlasRegion> getAnimation(Array<AtlasRegion> regions) {
		Animation<AtlasRegion> animation;
		animation = new Animation<>(0.033f, regions, PlayMode.NORMAL);
		return animation;
	}
	
	@Override
	public DefenceStage getStage() {
		return (DefenceStage)super.getStage();
	}
	
	public float getHitRadius()
	{
		return hitRadius;
	}

	public float getHitRadius2()
	{
		return hitRadius2;
	}

	@Override
	public float getCenterX()
	{
		return getX()+image.getOriginX();
	}
	
	@Override
	public float getCenterY()
	{
		return getY()+image.getOriginY();
	}

	@Override
	public Vector2 getCenter()
	{
		return P.getVector2(getCenterX(),getCenterY());
	}

	public boolean isAlive() {
		return alive;
	}
	
	/**
	 * 
	 * @param iterator Remove it from this if it dies
	 */
	public void die() {
		ismarkedForDeath = true;
	}

	/**
	 * This was the die() function. But if the procedure is called any time it always requires
	 * an iterator to iterate the list, the actor sits in. To remove the iterator everywhere
	 * the new die() funtion just marks it for death. In the next act() this method will be called.
	 */
	private void excuteDeathProcedure() {
		if (!alive)
			return; // die() can sometimes be called again (if shot by bullet, and killed again by blast dmg, or 2 blasts dmgs)
		this.alive = false;
		getStage().gm.addMoney(value);
		getStage().gm.addPoints(getPoints());
		
		// display explosion (must be done before remove, otherwise: stage == null)
		if (!isHuman)
		{
			Explosion ex = Pools.get(Explosion.class).obtain();
			ex.init(getStage(),getCenterX(),getCenterY(),1f,getHitRadius()*4f); // test to spawn at pointed location
		}
		// display bloodmark/burnmark on ground
		//Mark mark = Pools.get((isHuman && lastDamageType.isBloody())?BloodMark.class:BurnMark.class).obtain(); // unary doesn't work in J1.8
		Mark mark;
		if (isHuman && lastDamageType.isBloody())
			mark = Pools.get((BloodMark.class)).obtain();
		else
			mark = Pools.get((BurnMark.class)).obtain();
		mark.init(getStage(),getCenterX(),getCenterY(), getHitRadius());
		
		getStage().removeEnemy(this);
	}

	public int getHP() {
		return HP;
	}

	/**
	 * deal damage
	 * @param hp the amount of HP to remove from this enemy
	 */
	public void damage(int hp, DamageType dmgType) {
		HP -= hp;
		lastDamageType = dmgType;
		lastShotTimestamp = lifeTime;
		if (HP<=0)
			die();
		else
			updateHealthBar();
	}

	public void heal(int hp) {
		if (isAlive())
		{
			HP += hp;
			updateHealthBar();
		}
	}
	
	public void addAnnouncedDamage(int hp) {
		announcedDamage+=hp;
	}
	
	public boolean isDefinetelyGoingToDie() {
		return HP-announcedDamage <=0;
	}
	
	public float getHealthPercentage()
	{
		return (float)HP / (float)maxHP;
	}
	
	public Color getHealthBarColour() {
		return healthBarColor;
	}
	
	private void updateHealthBar() {
		setHealthBarVisible(true);
		final float healthPercentage = getHealthPercentage();
		// set colour of healthbar
		if (healthPercentage >=0.666666f)
		{
			healthBarColor=Color.LIME;
		}
		else
		{
			if (healthPercentage>=0.333333f)
			{
				healthBarColor=Color.ORANGE.cpy().lerp(Color.LIME, healthPercentage*1.5f);
			}
			else
			{
				healthBarColor=Color.RED.cpy().lerp(Color.ORANGE, healthPercentage*3f);
			}
		}
	}

	public boolean isHealthBarVisible() {
		return healthBarVisible;
	}

	public void setHealthBarVisible(boolean healthBarVisible) {
		this.healthBarVisible = healthBarVisible;
	}

	public void setIced(boolean iced) {
		this.isIced = iced;
		iceImage.setVisible(iced);
	}
	
	public void freezeTemporarily(float seconds)
	{
		setIced(true);
		timestampToUnfreeze = lifeTime+seconds;
	}

	/**
	 * Enemies of harder difficultly get more points.
	 * Higher difficultly means more HP have been shot down.
	 * Killing them earlier gives also more points.
	 * @return the amount of points to get if this enemy dies right now
	 */
	public long getPoints()
	{
		final float multiplier = 1f+(1f/getProgress());
		return (long) (maxHP*multiplier);
	}



    
    private void initPath()
    {
		
		// add variance
		catmullDataSet = new Vector2[way.wayPoints.size()];
		int j=0;
		for (WayPoint wp : way.wayPoints) {
			final float varianceX = MathUtils.random(-wp.v, wp.v);
			final float varianceY = MathUtils.random(-wp.v, wp.v);
			catmullDataSet[j++] = P.getVector2(wp.x+varianceX, wp.y+varianceY);
		}
		
        catmull = new CatmullRomSpline<>(catmullDataSet, false);
        
        // normalize speed
        speed /=way.wayPoints.size();
    }
    
    /**
     * Gets the progress (percentage) on the way to the goal
     * @return a value [0-1]
     */
	public float getProgress() {
		return progressOnPath;
	}
	
    public float getSpeed() {
    	if (isIced)
    		return speed/2f;
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	@Override
    public void act(float delta) {
    	super.act(delta);
    	lifeTime+=delta;
    	
    	//remove enemies that are marked for death now
    	if (ismarkedForDeath)
    	{
    		excuteDeathProcedure();
    		return; //this was the last time this actor acted
    	}
    	
    	//unbuff if buff time over
    	if (lifeTime>timestampToUnfreeze)
    		setIced(false);

    	// here comes the waypoint handling
    	if (progressOnPath==0)
    		catmull.derivativeAt(catmullOut, progressOnPath);
    	
        progressOnPath += (delta * getSpeed()) / catmullOut.len();
        
        if (progressOnPath >= 1)
        {
        	getStage().gm.reduceHealthByOne(); // reduce health of game
            progressOnPath -= 1;
        	getStage().removeEnemy(this);
        	return;
        }
        catmull.valueAt(catmullOut, progressOnPath);
        setPosition(catmullOut.x-image.getOriginX(), catmullOut.y-image.getOriginY());
        catmull.derivativeAt(catmullOut, progressOnPath);
        image.setRotation(catmullOut.angle());
        iceImage.setRotation(catmullOut.angle());
        
        // hide healthbar if still green and no hit since a long time
        if (lifeTime - lastShotTimestamp > VISIBLE_TIME_OF_GREEN_HEALTHBAR)
        {
        	if (HP/(float)maxHP > 0.5f)
        		setHealthBarVisible(false);
        }
    }

}
