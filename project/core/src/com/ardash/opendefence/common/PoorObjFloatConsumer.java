package com.ardash.opendefence.common;

/**
 * A \@FunctionalInterface for poor people.
 * It does the old lambda thing, because J1.8 breaks the game on old devices.
 * @author Andreas Redmer
 *
 * @param <T>
 */
public interface PoorObjFloatConsumer<T> {

    /**
     * Performs this operation on the given arguments.
     *
     * @param t     the first input argument
     * @param value the second input argument
     */
    void accept(T t, float value);
}