/*******************************************************************************
 * Copyright (C) 2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.common;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Pool.Poolable;

/**
 * This class is poolable but doesn't have to be pooled. 
 * The unpool() implementation can simply be empty.
 * @author ubuntu
 *
 */
public abstract class SelfDestructableImage extends Image implements Poolable
{
	protected float lifeTime = 0f;
	protected float timeToLive = 0f;
	protected boolean isSelfDestructive = false;
	public SelfDestructableImage(TextureRegion keyFrame) {
		super(keyFrame);
		// we need almost no touchable animated items, so we turn it off here
		// and turn it back on in the child class if needed.
		setTouchable(Touchable.disabled);
	}

	@Override
	public void reset()
	{
		lifeTime = 0f;
		timeToLive = 0f;
		isSelfDestructive = false;
	}
	
	/**
	 * free this object in its specific class pool
	 */
	protected abstract void free();
	
	protected void setTimeToLive(float d) {
		this.timeToLive = d;
		isSelfDestructive = true;
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);
		lifeTime += delta;
		if (isSelfDestructive && lifeTime>timeToLive)
		{
			// animation is over
			free();
			remove();
		}
	}


}
