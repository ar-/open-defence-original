/*******************************************************************************
 * Copyright (C) 2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.effects;

import com.ardash.opendefence.A;
import com.ardash.opendefence.A.ARAsset;
import com.ardash.opendefence.common.HasCenter;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.badlogic.gdx.utils.Pools;

public class BloodMark extends Mark implements Poolable, HasCenter{
	
	
	public BloodMark() {
		super(getRandomBloodmark());
		/*
		 * all images  must be designed twice as big, they are  scaled  down here
		 * that way they still look good when zoomed in, because max zoom is 0.5
		 */
		this.setScale(0.8f);
		setOrigin(getWidth()/2f, getHeight()/2f);
	}
	
    static TextureRegion getRandomBloodmark() {
		final Array<AtlasRegion> regions = A.getTextureRegions(ARAsset.BLOODMARK);
		return regions.random();
	}

	@Override
	protected void free() {
		Pools.get(BloodMark.class).free(this);
	}

	@Override
	protected TextureRegion getRandomMark() {
		return getRandomBloodmark();
	}

}