/*******************************************************************************
 * Copyright (C) 2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.effects;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;

import com.ardash.opendefence.A;
import com.ardash.opendefence.A.ARAsset;
import com.ardash.opendefence.DefenceStage;
import com.ardash.opendefence.P;
import com.ardash.opendefence.common.Enemy;
import com.ardash.opendefence.common.HasCenter;
import com.ardash.opendefence.common.SelfDestructableImage;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.badlogic.gdx.utils.Pools;

public class BulletHit extends SelfDestructableImage implements Poolable, HasCenter{
	
	
	public BulletHit() {
		super(A.getTextureRegion(ARAsset.BULLET_HIT));
		/*
		 * all images  must be designed twice as big, they are  scaled  down here
		 * that way they still look good when zoomed in, because max zoom is 0.5
		 */
		this.setScale(0.5f);
		setOrigin(getWidth()/2f, getHeight()/2f);
	}
	
    /**
     * Initialize the object. Call this method after getting a object from the pool.
     */
	@Deprecated
	public void init(DefenceStage stage, float x, float y)
	{
		//Gdx.app.log("BulletHit","init");
		
		setPosition(x-getOriginX(), y-getOriginY());
		stage.grp05smallParticle.addActor(this);
		addAction(fadeOut(0.12f));
		setTimeToLive(0.12f);
		setVisible(true);
	}

	public void init(Enemy e, float fromX, float fromY)
	{
		Vector2 from = new Vector2(fromX,fromY);
		Vector2 to = e.getCenter().cpy();
		float rot = to.sub(from).angle();
		to.setLength(e.getHitRadius()/2f).rotate90(0).rotate90(0).add(e.getCenter());
		setRotation(rot);
		fromX = e.getCenterX();
		fromY = e.getCenterY();
		// this will cause a NPE if enemy not on stage any more (dead)
		init (e.getStage(),to.x,to.y);
	}
	
    /**
     * Callback method when the object is freed. It is automatically called by Pool.free()
     * Must reset every meaningful field of this object.
     */
	@Override
	public void reset() {
		super.reset();
		//Gdx.app.log("BulletHit","reset");
		setVisible(false);
		getColor().a=1f;
		if (getActions().size >0)
		{
			removeAction(getActions().get(0));
		}
		getActions().clear();
		remove();
	}
	
	@Override
	protected void free() {
		Pools.get(BulletHit.class).free(this);
	}

	
	@Override
	public float getCenterX()
	{
		return getX()+getOriginX();
	}
	
	@Override
	public float getCenterY()
	{
		return getY()+getOriginY();
	}
	
	@Override
	public Vector2 getCenter()
	{
		return P.getVector2(getCenterX(),getCenterY());
	}


}