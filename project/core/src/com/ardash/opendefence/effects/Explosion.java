/*******************************************************************************
 * Copyright (C) 2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.effects;

import com.ardash.opendefence.A;
import com.ardash.opendefence.A.AtlasAsset;
import com.ardash.opendefence.A.SoundAsset;
import com.ardash.opendefence.DefenceStage;
import com.ardash.opendefence.P;
import com.ardash.opendefence.common.AnimatedImage;
import com.ardash.opendefence.common.HasCenter;
import com.ardash.opendefence.helpers.SoundPlayer;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.badlogic.gdx.utils.Pools;

public class Explosion extends AnimatedImage implements Poolable, HasCenter {
	
	/**
	 * this is the radius of a tiny explosion that just indicates a rocket hit
	 * Explosions smaller than this, will be drawn on the hit particle layer,
	 * so they don't draw over and mess up the big explosions
	 */
	public static float MINI_EXPLOSION_RADIUS=20f;
	public static float EQUIPMENT_EXPLOSION_RADIUS=200f;
	public Explosion() {
		super(getAnimation());
		looping = false;
		/*
		 * all images  must be designed twice as big, they are  scaled  down here
		 * that way they still look good when zoomed in, because max zoom is 0.5
		 */
		this.setScale(0.5f);
		setOrigin(getWidth()/2f, getHeight()/2f);
	}

	/**
	 * Helper method to be used in constructor.
	 * Creates a default exp animation that can be adjusted later.
	 * @return
	 */
	private static Animation<AtlasRegion> getAnimation() {
		final TextureAtlas explosionAtlas = A.getAtlas(AtlasAsset.EXPLOSION);
		Animation<AtlasRegion> expAnimation;
		expAnimation = new Animation<>(0.033f, explosionAtlas.findRegions("explosion"), PlayMode.NORMAL);
		return expAnimation;
	}

    /**
     * Initialize the object. Call this method after getting a object from the pool.
     * @param duration should be max 1.584 (equals 0.033f sec per frame for 48 frames)
     */
	public void init(DefenceStage stage, float x, float y, float duration, float radius)
	{
		//Gdx.app.log("Explosion","init");
		setPosition(x-getOriginX(), y-getOriginY());
		setRotation(MathUtils.random(360f));
		setTimeToLive(duration);
		setRadius(radius);
		if (radius<=MINI_EXPLOSION_RADIUS)
		{
			stage.grp06hitExplosions.addActor(this);
			// play sound
			final int rnd = MathUtils.random(2);
			switch (rnd) {
			case 0:
				SoundPlayer.playSound(A.getSound(SoundAsset.MISSILE_HIT1));
				break;
			case 1:
				SoundPlayer.playSound(A.getSound(SoundAsset.MISSILE_HIT2));
				break;
			case 2:
			default:
				SoundPlayer.playSound(A.getSound(SoundAsset.MISSILE_HIT3));
				break;
			}
		}
		else if (radius<=EQUIPMENT_EXPLOSION_RADIUS)
		{
			stage.grp07smallExplosions.addActor(this);
			// play sound
			final int rnd = MathUtils.random(2);
			switch (rnd) {
			case 0:
				SoundPlayer.playSound(A.getSound(SoundAsset.DIRT_EXPLOSION1));
				break;
			case 1:
			default:
				SoundPlayer.playSound(A.getSound(SoundAsset.DIRT_EXPLOSION2));
				break;
			}
		}
		else
			stage.grp11bigExplosions.addActor(this);
		setVisible(true);
		
	}

	private void setRadius(float radius) {
		final int frameHeightWight = 256;
		final int frameRadius = frameHeightWight/2;
		//radius/=2f;
		final float wantedScale = radius/(float)frameRadius;
		setScale(wantedScale*0.5f);
		setOrigin(getWidth()/2f, getHeight()/2f);
	}

	/**
     * Callback method when the object is freed. It is automatically called by Pool.free()
     * Must reset every meaningful field of this object.
     */
	@Override
	public void reset() {
		//Gdx.app.log("Explosion","reset");
		setVisible(false);
		remove();
		lifeTime = 0f;
		timeToLive = 0f;
		super.reset();
	}
	
	@Override
	protected void free() {
		Pools.get(Explosion.class).free(this);
	}

	@Override
	public float getCenterX()
	{
		return getX()+getOriginX();
	}
	
	@Override
	public float getCenterY()
	{
		return getY()+getOriginY();
	}
	
	@Override
	public Vector2 getCenter()
	{
		return P.getVector2(getCenterX(),getCenterY());
	}

}
