/*******************************************************************************
 * Copyright (C) 2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.effects;

import com.ardash.opendefence.A;
import com.ardash.opendefence.A.ParticleAsset;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;

public class IceEffect extends Group
{
	private ParticleEffect effect;
	private boolean isStarted = false;
	private ParticleEmitter emitter;
	private float scale =1.0f;
	
	/**
	 * the widest angle of the spray at the end of the animation
	 */
	private float openingAngle = 27.5510205f; // 90f*0.30612245f
	
	public IceEffect()
	{
		//TextureAtlas particleAtlas = A.getAtlas(AtlasAsset.ACTORS);
		effect = new ParticleEffect(A.getParticleEffect(ParticleAsset.ICE_ADDITIVE));
		//effect.load(Gdx.files.internal("ice_additive.p"), particleAtlas);
		effect.setDuration(500);

		final Array<ParticleEmitter> emitters = effect.getEmitters();
		emitter = emitters.first();
		
		// less particles to use less resources
//		emitter.setMaxParticleCount(50);
//		emitter.getEmission().setHigh(10,10);
//		startEffect();
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);
		if(isStarted)
		{
			// make particles emit endlessly
			emitter.durationTimer = 0;
		}
		effect.update(delta);

	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		effect.draw(batch);
	}
	
	@Override
	public void setPosition(float x, float y) {
		super.setPosition(x, y);
		effect.setPosition(x, y);
	}
	
	@Override
	public void setRotation(float degrees) {
		super.setRotation(degrees);
		emitter.getAngle().setHigh(degrees-openingAngle/2f, degrees+openingAngle/2f);
		emitter.getAngle().setLow(degrees);
		
	}
	
	public void startEffect()
	{
		isStarted = true;
		effect.start();
	}
	
	public void stopEffect()
	{
		isStarted = false;
	}
	
	@Override
	public boolean remove() {
		effect.dispose();
		return super.remove();
	}
	
	public float getScale() {
		return scale;
	}

	/** 
	 * set the scale in a revertable way
	 */
	@Override
	public void setScale(float s) {
		// first revert the old scale operation
		final float invScale =  1f/this.scale;
		effect.scaleEffect(invScale);
		this.scale = s;
		effect.scaleEffect(scale);
	}

	/**
	 * Set the scale of the fixed size particle system,
	 * to make it look like it shoots a certain range.
	 * r=300 -> scale = 1.0f
	 * 
	 * @param r
	 */
	public void setRange(float r)
	{
		// min range is 0 and max range is 300
		// normalize range:
		r/=300f;
		final float interpolatedScale = MathUtils.lerp(0f, 1f, r);
		setScale(interpolatedScale);
	}
}
