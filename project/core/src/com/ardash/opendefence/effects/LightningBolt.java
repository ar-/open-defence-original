/*******************************************************************************
 * Copyright (C) 2017,2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.effects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.ardash.opendefence.P;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Interpolation.SwingOut;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.badlogic.gdx.utils.Pools;

/**
 * A bunch of @see Line Put together to create ONE lightning.
 * This not not the projectile of a Tesla.
 * A Tesla shoots many lightnings.
 */
public class LightningBolt extends Actor implements Poolable{
	private float alpha;
	public List<Line> segments;

	private static final Color LIGHTNING_COLOR = new Color(0xa020f3ff); // purple
	private static final float alphaMultiplier = 3f;
	private static final float fadeOutRate = 0.09f;
	static Random rand = new Random(System.currentTimeMillis());

	// variables for self-destruction
	protected float lifeTime = 0f;
	protected final float timeToLive = 0.1f;

	public Vector2 Start(){
		return segments.get(0).pA;
	}
	public Vector2 End(){
		return segments.get(segments.size()-1).pB;
	}
	public boolean isComplete() {
		return (getAlpha() <=0);
	}

	private float getAlpha(){
		return alpha;
	}

	public void init(Vector2 source, Vector2 dest){
		segments = createBoltSegments(source, dest, 10);
		alpha = 1f;
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		draw((SpriteBatch)batch);
	} 
	
	public void draw(SpriteBatch spriteBatch)
	{
//		Gdx.app.log("Segments ", Segments.size() +" " );
		if (alpha <= 0)
			return;
		
        Color prevColor = spriteBatch.getColor();
        spriteBatch.setColor(P.getColor(LIGHTNING_COLOR).mul(alpha*alphaMultiplier));
        spriteBatch.setColor(P.getColor(LIGHTNING_COLOR));
        int blfn = spriteBatch.getBlendDstFunc();
        spriteBatch.setBlendFunction(spriteBatch.getBlendSrcFunc(), GL20.GL_ONE);
		
		for(int i=0; i<segments.size(); i++) {
			Line segment = segments.get(i);
			segment.Draw(spriteBatch, null);
		}
		
        spriteBatch.setColor(prevColor);
        spriteBatch.setBlendFunction(spriteBatch.getBlendSrcFunc(), blfn);

	}

	@Override
	public void act(float delta) {
		super.act(delta);
		lifeTime += delta;
		if (lifeTime>timeToLive)
		{
			// animation is over
			free();
			remove();
		}
		alpha -= fadeOutRate;
	}
	
	public void free()
	{
		Pools.get(LightningBolt.class).free(this);
	}
	
	protected static List<Line> createBoltSegments(Vector2 source, Vector2 dest, float thickness)
	{
		List<Line> results = new ArrayList<>();
		//Vector2 tangent = P.getVector2(dest).sub(P.getVector2(source));
		Vector2 tangent = P.getVector2(dest).sub(source);
		Vector2 normal = P.getVector2(tangent.y, -tangent.x).nor();
		float length = tangent.len();

		List<Float> positions = new ArrayList<>();
		positions.add(0f);
		for (int i = 0; i < length / 4; i++)
			positions.add(Rand(0, 1));

		Collections.sort(positions);

		float Sway = 500;
		float Jaggedness = 1 / Sway;

//		Interpolation ip = Interpolation.swingOut;
		Interpolation ip = new SwingOut(1.5f);
		Vector2 prevPoint = source;
		float prevDisplacement = 0;
		final Vector2 tangentMovement = P.getVector2();
		final Vector2 normalMovement = P.getVector2();
		for (int i = 1; i < positions.size(); i++)
		{
			float pos = positions.get(i);

			// used to prevent sharp angles by ensuring very close positions also have small perpendicular variation.
			float scale = (length * Jaggedness) * (pos - positions.get(i-1));

			// defines an envelope. Points near the middle of the bolt can be further from the central line.
			float envelope = pos > 0.95f ? 20 * (1 - pos) : 1;

			float displacement = Rand(-Sway, Sway);
			displacement -= (displacement - prevDisplacement) * (1 - scale);
			displacement *= envelope;

			tangentMovement.set(0,0);
			normalMovement.set(0,0);
			Vector2 point =P.getVector2(source).add(tangentMovement.mulAdd(tangent, pos)).add(normalMovement.mulAdd(normal, displacement)) ;
			float minThickness=1;
			float maxThickness=  thickness;
			final float thick = ip.apply(minThickness, maxThickness, i/(float)positions.size());
			results.add(P.getLine(prevPoint, point, thick));
			prevPoint = point;
			prevDisplacement = displacement;
		}

		results.add(P.getLine(P.getVector2(prevPoint), P.getVector2(dest), thickness));

		P.free(tangent, normal, tangentMovement, normalMovement);
		return results;
	}

	private Line find(List<Line> list, Vector2 start, Vector2 dir, float position){
		// Find (x => Vector2.Dot(x.B - start, dir) >= position
		
		for(int i=0; i<list.size(); i++){
			Vector2 sub1 = list.get(i).pB.sub(start);
			float dot = Vector2.dot(sub1.x, sub1.y, dir.x, dir.y);
			if (dot >= position){
				return list.get(i);
			}
		}
		if (list.size() > 0)
			return list.get(0);
		return null;
	}
	
	public Vector2 getCenter()
	{
		return segments.get(segments.size()/2).pA;
	}
	
	public Vector2 getSplitPoint()
	{
		return segments.get((int)(segments.size()*.66f)).pA;
	}
	
	// Returns the point where the bolt is at a given fraction of the way through the bolt. Passing
	// zero will return the start of the bolt, and passing 1 will return the end.
	public Vector2 GetPoint(float position)
	{
		Vector2 start = Start();
		Vector2 end = End();
		float length = Vector2.dst(start.x, start.y, end.x, end.y);
		Vector2 dir = P.getVector2().mulAdd(end.sub(start), 1/length);
		position *= length;

		Line line = find(segments, start, dir, position);
		Vector2 dot1 = line.pA.sub(start);
		Vector2 dot2 = line.pB.sub(start);
		float lineStartPos = Vector2.dot(dot1.x, dot1.y,dir.x, dir.y);
		float lineEndPos = Vector2.dot(dot2.x, dot1.y,dir.x, dir.y);
		float linePos = (position - lineStartPos) / (lineEndPos - lineStartPos);
		return line.pA.lerp(line.pB, linePos);
	}

	private static float Rand(float min, float max)
	{
		return (float)rand.nextDouble() * (max - min) + min;
	}

	@Override
	public void reset() {
		alpha = 0f;
		for (Line line : segments) {
			P.free(line);
		}
		segments.clear();
		segments = null;
		lifeTime = 0f;
	}
}
