/*******************************************************************************
 * Copyright (C) 2017,2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.effects;

import com.ardash.opendefence.A;
import com.ardash.opendefence.A.ARAsset;
import com.ardash.opendefence.P;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool.Poolable;

/**
 * A line (optionally with parts for begin and end) drawn from textures.
 * o be used for lightnings.
 */
public class Line implements Poolable{

    public Vector2 pA;
    public Vector2 pB;
    public float myThickness;
	private static TextureRegion LightningSegment;

    public Line() { 
    	// the first constructor call can init the statics
    	// this actor should not search the regions all the time, because it gets created a lot
    	LightningSegment =A.getTextureRegion(ARAsset.LIGHTNINGSEGMENT);
    }
    
	/**
	 * @param a
	 * @param b
	 * @param thickness
	 */
    public void init(Vector2 a, Vector2 b, float thickness) {
		pA = P.getVector2(a);
		pB = P.getVector2(b);
		myThickness = thickness;
	}

    public void Draw(SpriteBatch spriteBatch, Color tint)
    {
            Vector2 tangent = P.getVector2(pB).sub(P.getVector2(pA));
            float theta = (float)Math.toDegrees(Math.atan2(tangent.y, tangent.x));
            spriteBatch.draw(LightningSegment, pA.x, pA.y, 0,myThickness/2, getLength(), myThickness, 1,1, theta);
            P.free(tangent);
    }

    public float getLength(){
    	float len = (float)Math.sqrt((pA.x - pB.x)*(pA.x - pB.x) + (pA.y - pB.y)*(pA.y - pB.y));
    	return len;
    }
    
	@Override
	public void reset() {
        pA = P.getVector2();
        pB = P.getVector2();
        myThickness=0;
	}
}
