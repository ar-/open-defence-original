package com.ardash.opendefence.effects;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.ardash.opendefence.DefenceStage;
import com.ardash.opendefence.P;
import com.ardash.opendefence.common.SelfDestructableImage;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public abstract class Mark extends SelfDestructableImage {

	private static final float BURN_MARK_LIFE_TIME = 13.12f;

	public Mark(TextureRegion keyFrame) {
		super(keyFrame);
	}

	/**
	 * Initialize the object. Call this method after getting a object from the pool.
	 */
	public void init(DefenceStage stage, float x, float y, float size) {
		//Gdx.app.log("Mark","init");
		
		setSize(size*4f, size*4f);
		//setScale(0.8f); //scale in child class
		setOrigin(getWidth()/2f, getHeight()/2f);
		setPosition(x-getOriginX(), y-getOriginY());
		setRotation(MathUtils.random(360f));
		stage.grp02ash.addActor(this);
		addAction(sequence(delay(BURN_MARK_LIFE_TIME*.9f),fadeOut(BURN_MARK_LIFE_TIME*.1f)));
		setTimeToLive(BURN_MARK_LIFE_TIME);
		setVisible(true);
	}

	/**
	 * Callback method when the object is freed. It is automatically called by Pool.free()
	 * Must reset every meaningful field of this object.
	 */
	@Override
	public void reset() {
		super.reset();
		//Gdx.app.log("Mark","reset");
		setVisible(false);
		getColor().a=1f;
		if (getActions().size >0)
		{
			removeAction(getActions().get(0));
		}
		getActions().clear();
		remove();
		
	    ((TextureRegionDrawable)getDrawable()).setRegion(getRandomMark());
	}

	protected abstract TextureRegion getRandomMark();

	public float getCenterX() {
		return getX()+getOriginX();
	}

	public float getCenterY() {
		return getY()+getOriginY();
	}

	public Vector2 getCenter() {
		return P.getVector2(getCenterX(),getCenterY());
	}

}