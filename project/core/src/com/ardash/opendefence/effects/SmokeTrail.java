/*******************************************************************************
 * Copyright (C) 2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.effects;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.color;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;

import com.ardash.opendefence.A;
import com.ardash.opendefence.A.ARAsset;
import com.ardash.opendefence.DefenceStage;
import com.ardash.opendefence.P;
import com.ardash.opendefence.common.HasCenter;
import com.ardash.opendefence.common.SelfDestructableImage;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.badlogic.gdx.utils.Pools;

public class SmokeTrail extends SelfDestructableImage implements Poolable, HasCenter{
	
	
	private static final float SMOKE_TRAIL_LIFE_TIME = 1.12f;

	public SmokeTrail() {
		super(A.getTextureRegion(ARAsset.SMOKE_TRACE));
		/*
		 * all images  must be designed twice as big, they are  scaled  down here
		 * that way they still look good when zoomed in, because max zoom is 0.5
		 */
		this.setScale(0.5f);
		setOrigin(getWidth()/2f, getHeight()/2f);
	}
	
    /**
     * Initialize the object. Call this method after getting a object from the pool.
     */
	public void init(DefenceStage stage, float x, float y)
	{
		//Gdx.app.log("SmokeTrail","init");
		
		setScale(0.15f); // TODO scale smoke trail correctly
		setOrigin(getWidth()/2f, getHeight()/2f);
		setPosition(x-getOriginX(), y-getOriginY());
		setRotation(MathUtils.random(360f));
		stage.grp08bigParticle.addActor(this);
		addAction(fadeOut(SMOKE_TRAIL_LIFE_TIME));
		addAction(scaleTo(getScaleX()*2f, getScaleX()*2f, SMOKE_TRAIL_LIFE_TIME));
		//addAction(rotateTo(getRotation()+MathUtils.random(-10f,10f),SMOKE_TRAIL_LIFE_TIME));
		addAction(color(Color.WHITE, 0.1f));
		setTimeToLive(SMOKE_TRAIL_LIFE_TIME);
		setColor(Color.YELLOW);
		setVisible(true);
	}

    /**
     * Callback method when the object is freed. It is automatically called by Pool.free()
     * Must reset every meaningful field of this object.
     */
	@Override
	public void reset() {
		super.reset();
		//Gdx.app.log("SmokeTrail","reset");
		setVisible(false);
		getColor().a=1f;
		if (getActions().size >0)
		{
			removeAction(getActions().get(0));
		}
		getActions().clear();
		remove();
	}
	
	@Override
	protected void free() {
		Pools.get(SmokeTrail.class).free(this);
	}

	
	@Override
	public float getCenterX()
	{
		return getX()+getOriginX();
	}
	
	@Override
	public float getCenterY()
	{
		return getY()+getOriginY();
	}
	
	@Override
	public Vector2 getCenter()
	{
		return P.getVector2(getCenterX(),getCenterY());
	}


}