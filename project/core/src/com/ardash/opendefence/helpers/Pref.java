/*******************************************************************************
 * Copyright (C) 2017 Andreas Redmer <andreas.redmer@posteo.de>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

/**
 * Persistant Prefenrences
 *
 */
public class Pref {
	private static final String PREFS_NAME = "OpenDefencePrefs"; 
	private static final String PEPPER = "tp6UgD9JKAoWSww1Ogduts2UFlFn9CTqQJX8Zdw7VBZv0o51jKD9QTnuoxzA2Lcj"; 
	private static final Preferences prefs = Gdx.app.getPreferences(PREFS_NAME);
	
	private static Boolean soundOn = null;
	private static Float musicVol = null;
	private static Float soundVol = null;
	private static String player1name = null;
	private static String player2name = null;
	
	/**
	 * The selected language. Overrides the system language if selected.
	 * System language is "".
	 */
	private static String lingo = null;
//	private static HashMap<Mission,Boolean> activatedLevels = new HashMap<Mission, Boolean>();

	
	public static boolean getSoundOn() {
		if (soundOn == null)
		{
			//Preferences prefs = Gdx.app.getPreferences(PREFS_NAME);
			soundOn = prefs.getBoolean("soundOn", true); 
		}
		return soundOn;
	}
	
	public static void setSoundOn(boolean asoundOn) {
		soundOn = asoundOn;
//		GameManager gm = GameManager.getInstance();
//		
//		// stop musics that maybe playing in the background
//		if (!soundOn)
//		{
////			SoundPlayer.stopMusic(A.getMusic(MusicAsset.BG));
//		}
//		else
//		{
//			// don't start the music in game (but only in menues)
////			if (gm.getGameState() != GameState.WAIT_FOR_DRAG && gm.getGameState() != GameState.WAIT_FOR_PHYSICS && gm.getGameState() != GameState.DRAGGING)
//				SoundPlayer.playMusic(A.getMusic(MusicAsset.BG));
//		}
			
		
		//Preferences prefs = Gdx.app.getPreferences(PREFS_NAME);
		prefs.putBoolean("soundOn", soundOn); 
		prefs.flush();
	}
	
	public static String getLingo() {
		if (lingo == null)
		{
			lingo = prefs.getString("lingo", "");
		}
		return lingo;
	}
	
	public static void setLingo(String alingo) {
		lingo = alingo;
		prefs.putString("lingo", lingo);
		prefs.flush();
	}

//	public static boolean isMissionActivated(Mission mission) {
//		
//		if (mission.getMinor() ==1)
//			return true; // minor=1 can't be deactivated
//		
//		Boolean act = activatedLevels.get(mission);
//		if (act == null)
//		{
//			final String key = Calculator.md5(PEPPER+"Activated"+mission.name()); 
//			act = prefs.getBoolean(key, false);
//			activatedLevels.put(mission,act);
//		}
//		return act;
//	}

//	public static void setMissionActivated(Mission mission, boolean activated) {
//		activatedLevels.put(mission,activated);
//		final String key = Calculator.md5(PEPPER+"Activated"+mission.name()); 
//		prefs.putBoolean(key, activated);
//		prefs.flush();
//	}

}
