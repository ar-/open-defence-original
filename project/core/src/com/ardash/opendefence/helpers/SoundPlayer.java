/*******************************************************************************
 * Copyright (C) 2015-2017 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.helpers;

import com.ardash.opendefence.A;
import com.ardash.opendefence.A.SoundAsset;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

/**
 * Wrapper for play sound and music. Obeys the Sound-on-off-setting
 */
public class SoundPlayer {
	

	public static long playSound(SoundAsset soundAsset)
	{
		return playSound(A.getSound(soundAsset));
	}
	
	public static long playSound(Sound sound)
	{
		return playSound(sound, false);
	}
	
	public static long playSound(Sound sound, boolean loop)
	{
		if (sound == null)
				return -1;
		if (Pref.getSoundOn())
		{
			if (loop)
			{
				return sound.loop();
			}
			return sound.play();
		}
		return -1;
	}
	
	public static void stopSound(Sound sound)
	{
		if (sound == null)
				return;
		if (Pref.getSoundOn())
			sound.stop();
	}
	
	public static void stopSound(Sound sound, long soundId)
	{
		if (sound == null)
				return;
		if (Pref.getSoundOn())
			sound.stop(soundId);
	}
	
	public static void playSound(Sound sound, float volume)
	{
		if (sound == null)
			return;
		if (Pref.getSoundOn())
			sound.play(volume);
	}

	public static void playMusic(Music music)
	{
		if (music == null)
			return;
		if (Pref.getSoundOn() && !music.isPlaying())
		{
			music.play();
			music.setLooping(true);
		}
		//music.setVolume(Pref.getMusicVol());
	}

	public static void stopMusic(Music music)
	{
		if (music == null)
			return;
		if (music.isPlaying())
			music.stop();
	}
	
	public static void pauseMusic(Music music)
	{
		if (music == null)
			return;
			music.pause();
	}
}
