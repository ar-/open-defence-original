/*******************************************************************************
 * Copyright (C) 2017 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.helpers;

import java.util.Comparator;
import java.util.concurrent.PriorityBlockingQueue;

import com.ardash.opendefence.A;
import com.ardash.opendefence.DefenceStage;
import com.ardash.opendefence.GameManager;
import com.ardash.opendefence.beans.ETD;
import com.ardash.opendefence.beans.SpawnDescriptor;
import com.ardash.opendefence.beans.Wave;
import com.ardash.opendefence.beans.Way;
import com.ardash.opendefence.beans.WayList;
import com.ardash.opendefence.common.Enemy;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Array;

/**
 * an invisible actor-like thing, that sits on a stage and spawns new enemies
 * 
 * fetches the WayList only once
 * and fetches each Wave from the WaveList one demand
 * 
 * @author Andreas Redmer
 *
 */
public class Spawner {
	
	private static final Comparator<SpawnDescriptor> spawnDescriptorComparator = new Comparator<SpawnDescriptor>() {
		@Override
		public int compare(SpawnDescriptor o1, SpawnDescriptor o2) {
			return Float.compare(o1.start, o2.start);
		}
	};

	private final DefenceStage stage;
	private final GameManager gm;
	private final WayList wayList;
	private Wave wave;
	private final PriorityBlockingQueue<SpawnDescriptor> q;
	private float waveTime;

	public Spawner(DefenceStage gameStage) {
		this.stage = gameStage;
		this.gm = gameStage.gm;
		this.wayList = gm.getListOfWays();
		wave =null;
		q = new PriorityBlockingQueue<>(1,spawnDescriptorComparator);
	}

	public void act(float delta) {
		waveTime += delta;
		if (wave == null)
		{
			wave = gm.getWave();
			waveTime = 0;
			
			for (SpawnDescriptor spawnDescriptor : wave)
			{
				// wave contains "sub-waves" packed in spawn descriptors
				// spawn descriptors have start time, amount and delay
				// generate from that, new (copies) of spawn descriptors with: amount = 1
				// and start time + added delay
				for (int i=0 ; i< spawnDescriptor.amount ; i++)
				{
					SpawnDescriptor sd = new SpawnDescriptor(spawnDescriptor);
					sd.amount=1; // will be ignored anyway
					sd.start += sd.delay * i;
					q.put(sd);
				}
			}
		}
		
		if (!q.isEmpty())
		{
			// check if the time has come for the next one to spawn
			// if so, pop from queue and initiate the spawn
			// hint: we pop only max one per frame
			if (q.peek().start<waveTime)
			{
				final SpawnDescriptor sd = q.poll();
				spawnEnemy(sd);
			}
		}
		
		// now q contains every single enemy for this wave
		
	}

	private void spawnEnemy(SpawnDescriptor sd) {
		final Way way = wayList.get(sd.wayIndex);
		final ETD etd = gm.getEnemyTypeDescriptor(sd.etid);
		final Array<AtlasRegion> regions = A.getTextureRegions(etd.textureName);
		
		Enemy e = new Enemy(regions, etd.hp, etd.speed, way, etd.value, etd.isHuman);
		stage.addEnemy(e);
	}
	
	/** 
	 * One precondition to bring up the next wave (or win the game) is that the current wave is empty.
	 * @return true if there are no more enemies to be spawned in this wave
	 */
	public boolean isWaveEmpty() {
		return q.isEmpty();
	}
	
	public void triggerNewWave()
	{
		wave = null;
	}

}
