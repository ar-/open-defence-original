/*******************************************************************************
 * Copyright (C) 2017, 2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.screens;

import com.ardash.opendefence.A;
import com.ardash.opendefence.A.ARAsset;
import com.ardash.opendefence.A.MusicAsset;
import com.ardash.opendefence.A.SoundAsset;
import com.ardash.opendefence.DefenceStage;
import com.ardash.opendefence.GameManager;
import com.ardash.opendefence.OpenDefenceGame;
import com.ardash.opendefence.P;
import com.ardash.opendefence.common.Enemy;
import com.ardash.opendefence.common.ValueChangeListener;
import com.ardash.opendefence.effects.BulletHit;
import com.ardash.opendefence.effects.Explosion;
import com.ardash.opendefence.effects.SmokeTrail;
import com.ardash.opendefence.helpers.SoundPlayer;
import com.ardash.opendefence.screens.gui.ControlButton;
import com.ardash.opendefence.screens.gui.FpsIndicator;
import com.ardash.opendefence.screens.gui.TurretWindow;
import com.ardash.opendefence.screens.gui.UiStage;
import com.ardash.opendefence.screens.gui.WeaponButton;
import com.ardash.opendefence.turrets.Freezer;
import com.ardash.opendefence.turrets.Gatling;
import com.ardash.opendefence.turrets.Mortar;
import com.ardash.opendefence.turrets.MultiRpg;
import com.ardash.opendefence.turrets.Rpg;
import com.ardash.opendefence.turrets.Tesla;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Pools;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class GameScreen implements Screen , GestureListener {
	
	public static final int TEX_SIZE = 1024; // max texture size must be 1024x1024 or higher
	//public static final int TEX_SIZE = 2048; // max texture size must be 2048x2048 or higher

    final DefenceStage gameStage;
    public final UiStage uiStage;
    private final OpenDefenceGame game;
    private final OrthographicCamera camera;
    private final GameManager gm;

    private final InputMultiplexer multiplexer;
	Image validDropArea;
	
	private boolean paused = false;
	private boolean fastForward = false;

    public GameScreen(OpenDefenceGame aGame, String mapToLoad) {
    	// TODO change line around street in map 1 to brighter one
    	// TODO change shading or texture of metal base (and make it smaller)
    	System.gc();
        game = aGame;
        gm = aGame.gm;
        gm.loadMap(mapToLoad);
        uiStage = new UiStage(new ScreenViewport());
        gameStage = new DefenceStage(new ExtendViewport(TEX_SIZE,TEX_SIZE), aGame);
        
        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(uiStage);
        multiplexer.addProcessor(new GestureDetector(this));
        multiplexer.addProcessor((gameStage));

        Image map = new Image(new Texture("maps/"+mapToLoad+".jpg")); // TODO remove texture from Map bean
        validDropArea = new Image(new Texture("maps/"+mapToLoad+"_a.png"));
        
        map.setScale(0.5f);
        //validDropArea.setScale(0.5f);
        gameStage.grp00background.addActor(map);
		validDropArea.setVisible(false);
        gameStage.addDropArea(validDropArea);

        if (Gdx.files.internal("maps/"+mapToLoad+"_t.png").exists())
        {
        	// add tree layer if available
            Image trees = new Image(new Texture("maps/"+mapToLoad+"_t.png"));
            trees.setScale(0.5f);
            gameStage.grp10trees.addActor(trees);
        }

        generateGui();

        camera = (OrthographicCamera) gameStage.getViewport().getCamera();
        camera.translate(TEX_SIZE/2,TEX_SIZE/2);
        camera.zoom = 1.0f;
        
        map.addListener(new InputListener() {
        	@Override
        	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        		// click on map unselects all turrets
        		gameStage.setCurrentSelectedTurret(null);
        		return false;
        	}
        });
        
        setPaused(true);
    }

	/**
	 * 
	 */
	private void generateGui() {
		if (GameManager.renderFPSIndicator)
			uiStage.addActor(new FpsIndicator(game));
		
		uiStage.addTurretWindow(new TurretWindow(A.getSkin()));
        
        final Label moneyIndicator = new Label("000 $", A.getSkin(),"title");
        moneyIndicator.setWidth(Gdx.graphics.getWidth());
        final Label waveIndicator = new Label("Wave 0/0", A.getSkin(),"title");
        waveIndicator.setWidth(Gdx.graphics.getWidth());
        final Label healthIndicator = new Label("Health 20", A.getSkin(),"title");
        healthIndicator.setWidth(Gdx.graphics.getWidth());
        final Label pointsIndicator = new Label("Points 0", A.getSkin(),"title");
        pointsIndicator.setWidth(Gdx.graphics.getWidth());
        // TODO add indicator about percentage of wave
        // TODO add buttons for music and sound off
        
    	Table topRow = new Table();
    	topRow.setDebug(GameManager.renderGuiDebug);
    	final int pad = 20;
    	topRow.add(moneyIndicator).padRight(pad);
    	topRow.add(waveIndicator).padRight(pad);
    	topRow.add(healthIndicator).padRight(pad);
    	topRow.add(pointsIndicator);
    	

    	Table bottomRow = new Table();
    	bottomRow.setDebug(GameManager.renderGuiDebug);
    	bottomRow.setWidth(Gdx.graphics.getWidth());

    	final WeaponButton btnW0 = new WeaponButton(A.getSkin(), A.getTextureRegion(ARAsset.ICON_GATLING), Gatling.getBaseCost());
    	btnW0.addListener(new MenuWeaponListener(this, Gatling.class.getName()));
		bottomRow.add(btnW0).padRight(pad).width(80);
    	final WeaponButton btnW1 = new WeaponButton(A.getSkin(), A.getTextureRegion(ARAsset.ICON_RPG), Rpg.getBaseCost());
    	btnW1.addListener(new MenuWeaponListener(this, Rpg.class.getName()));
		bottomRow.add(btnW1).padRight(pad).width(80);
    	final WeaponButton btnW2 = new WeaponButton(A.getSkin(), A.getTextureRegion(ARAsset.ICON_TESLA), Tesla.getBaseCost());
    	btnW2.addListener(new MenuWeaponListener(this, Tesla.class.getName()));
		bottomRow.add(btnW2).padRight(pad).width(80);
    	final WeaponButton btnW3 = new WeaponButton(A.getSkin(), A.getTextureRegion(ARAsset.ICON_ICE), Freezer.getBaseCost());
    	btnW3.addListener(new MenuWeaponListener(this, Freezer.class.getName()));
		bottomRow.add(btnW3).padRight(pad).width(80);
    	@SuppressWarnings("static-access")
		final WeaponButton btnW4 = new WeaponButton(A.getSkin(), A.getTextureRegion(ARAsset.ICON_2ROCKETS), MultiRpg.getBaseCost());
    	btnW4.addListener(new MenuWeaponListener(this, MultiRpg.class.getName()));
		bottomRow.add(btnW4).padRight(pad).width(80);
    	final WeaponButton btnW5 = new WeaponButton(A.getSkin(), A.getTextureRegion(ARAsset.ICON_MORTAR), Mortar.getBaseCost());
    	btnW5.addListener(new MenuWeaponListener(this, Mortar.class.getName()));
		bottomRow.add(btnW5).padRight(pad).width(80);

		bottomRow.add().expandX().fillX().width(350);
		
		final ControlButton btnP = new ControlButton(A.getSkin(), A.getTextureRegion(ARAsset.ICON_PAUSE));
		bottomRow.add(btnP).padRight(pad).width(50);
		final ControlButton btnPl = new ControlButton(A.getSkin(), A.getTextureRegion(ARAsset.ICON_PLAY));
		bottomRow.add(btnPl).padRight(pad).width(50);
		final ControlButton btnFF = new ControlButton(A.getSkin(), A.getTextureRegion(ARAsset.ICON_FORWARD));
		bottomRow.add(btnFF).padRight(pad).width(50);
    	
    	Table table = new Table();
    	table.setDebug(GameManager.renderGuiDebug);
    	table.setFillParent(true);
    	table.add(topRow);
    	table.row();
    	table.add().expandY();
    	table.row();
    	table.add(bottomRow).expandX();

    	uiStage.addActor(table);

    	// action listeners
    	btnP.addListener(new InputListener() {
    		@Override
    		public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
    	        setPaused(false);
    			fastForward = false;
    			pause();
    			btnP.makeLookActiveSelection(true);
    			btnPl.makeLookActiveSelection(false);
    			btnFF.makeLookActiveSelection(false);
    			return true;
    		}
    	});
        
    	btnPl.addListener(new InputListener() {
    		@Override
    		public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
		        setPaused(false);
    			fastForward = false;
    			btnP.makeLookActiveSelection(false);
    			btnPl.makeLookActiveSelection(true);
    			btnFF.makeLookActiveSelection(false);
    			return true;
    		}
    	});
        
    	btnFF.addListener(new InputListener() {
    		@Override
    		public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
    	        setPaused(false);
    			fastForward = true;
    			btnP.makeLookActiveSelection(false);
    			btnPl.makeLookActiveSelection(false);
    			btnFF.makeLookActiveSelection(true);
    			return true;
    		}
    	});
    	
    	gm.setMoneyChangedListener(new ValueChangeListener<Integer>() {
			@Override
			public void onValueChange(Integer val) {
				moneyIndicator.setText(val + " $");
			}
		});
        
    	gm.setWaveChangedListener(new ValueChangeListener<Integer>() {
			@Override
			public void onValueChange(Integer val) {
				waveIndicator.setText("Wave: "+(val+1)+" of " + gm.getAmountOfWaves());
			}
		});
        
    	gm.setHealthChangedListener(new ValueChangeListener<Integer>() {
			@Override
			public void onValueChange(Integer val) {
				healthIndicator.setText("health "+val);
			}
		});
    	
    	gm.setPointsChangedListener(new ValueChangeListener<Long>() {
			@Override
			public void onValueChange(Long val) {
				pointsIndicator.setText("Points "+val);
			}
		});
    	
        
	}

    @Override
    public void show() {
        //Gdx.app.log("GameScreen","show");
        Gdx.input.setInputProcessor(multiplexer);
        SoundPlayer.stopMusic(A.getMusic(MusicAsset.INDUSTRIAL_96));
        
        gameStage.getUiStage(); // init the ui stage reference

    }

    @Override
    public void render(float delta) {
    	// handle inputs
    	if (Gdx.input.isKeyPressed(Keys.W))
		{
    		final Vector2 v = P.getVector2(0,10);
			camera.translate(v);
			P.free(v);
		}
    	if (Gdx.input.isKeyPressed(Keys.S))
		{
    		final Vector2 v = P.getVector2(0,-10);
			camera.translate(v);
			P.free(v);
		}
    	if (Gdx.input.isKeyPressed(Keys.A))
		{
    		final Vector2 v = P.getVector2(-10,0);
			camera.translate(v);
			P.free(v);
		}
    	if (Gdx.input.isKeyPressed(Keys.D))
		{
    		final Vector2 v = P.getVector2(10,0);
			camera.translate(v);
			P.free(v);
		}
    	if (Gdx.input.isKeyPressed(Keys.E))
		{
    		camera.zoom+=0.01f;
            //Gdx.app.log("zoom", ""+camera.zoom);
		}
    	if (Gdx.input.isKeyPressed(Keys.Q))
		{
    		camera.zoom-=0.01f;
            //Gdx.app.log("zoom", ""+camera.zoom);
		}
    	if (Gdx.input.isKeyJustPressed(Keys.P))
		{
    		// print current mouse position as waypoint
    		final Vector2 screenPoint = P.getVector2(Gdx.input.getX(),Gdx.input.getY());
			final Vector2 mp = gameStage.screenToStageCoordinates(screenPoint);
    		System.out.println("\t\t\t{ x: "+mp.x+", y: "+mp.y+", v: 5 }");
    		P.free(screenPoint, mp);
		}
    	if (Gdx.input.isKeyJustPressed(Keys.O))
		{
//            Enemy tank1 = new Tank1(gm.getListOfWays().get(0));
//            tank1.setPosition(300,600);
//            gameStage.addEnemy(tank1);
		}
    	if (Gdx.input.isKeyJustPressed(Keys.B)) // bullet hit (show)
		{
    		// print current mouse position as waypoint
    		final Vector2 screenPoint = P.getVector2(Gdx.input.getX(),Gdx.input.getY());
			final Vector2 mp = gameStage.screenToStageCoordinates(screenPoint);
    		//System.out.println("\t\t\t{ x: "+mp.x+", y: "+mp.y+", v: 5 }");
    		
    		BulletHit bh = Pools.get(BulletHit.class).obtain();
    		//bh.init(gameStage,mp.x,mp.y); // test to spawn at pointed location
    		Enemy e = gameStage.getEnemies().get(0);
    		bh.init(e, mp.x, mp.y);
    		P.free(screenPoint, mp);
		}
    	if (Gdx.input.isKeyJustPressed(Keys.N)) // explosion (show)
		{
    		// print current mouse position as waypoint
    		final Vector2 screenPoint = P.getVector2(Gdx.input.getX(),Gdx.input.getY());
			final Vector2 mp = gameStage.screenToStageCoordinates(screenPoint);
    		//System.out.println("\t\t\t{ x: "+mp.x+", y: "+mp.y+", v: 5 }");
    		
    		Explosion ex = Pools.get(Explosion.class).obtain();
    		ex.init(gameStage,mp.x,mp.y,1f,68f); // test to spawn at pointed location
    		//Enemy e = gameStage.getEnemies().get(0);
    		//bh.init(e, mp.x, mp.y);
    		P.free(screenPoint, mp);
		}
    	if (Gdx.input.isKeyJustPressed(Keys.M)) // smoke (show)
		{
    		// print current mouse position as waypoint
    		final Vector2 screenPoint = P.getVector2(Gdx.input.getX(),Gdx.input.getY());
			final Vector2 mp = gameStage.screenToStageCoordinates(screenPoint);
    		//System.out.println("\t\t\t{ x: "+mp.x+", y: "+mp.y+", v: 5 }");
    		
			SmokeTrail ex = Pools.get(SmokeTrail.class).obtain();
    		ex.init(gameStage,mp.x,mp.y); // test to spawn at pointed location
    		P.free(screenPoint, mp);
		}
    	

    	
    	camera.zoom = MathUtils.clamp(camera.zoom, 0.3f, TEX_SIZE/camera.viewportWidth);
    	float camViewportHalfX = camera.viewportWidth *0.5f * camera.zoom ;
    	float camViewportHalfY = camera.viewportHeight *0.5f * camera.zoom;
    	camera.position.x = MathUtils.clamp(camera.position.x, camViewportHalfX, TEX_SIZE - camViewportHalfX);
    	camera.position.y = MathUtils.clamp(camera.position.y, camViewportHalfY, TEX_SIZE - camViewportHalfY);

    	camera.update();
    	// draw // TODO remove one?
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        uiStage.act();
        
        if (!paused)
        {
        	gameStage.act();
	        if (fastForward)
	        	gameStage.act();
        }

        gameStage.draw();
        uiStage.draw();
    } 


	/**
	 * @param x
	 * @param y
	 * @return
	 */
	Vector2 transformGuiCoordToGameCoord(float x, float y) {
		final Vector2 guiCoords = P.getVector2(x, y);
		return transformGuiCoordToGameCoord(guiCoords);
	}

	/**
	 * @param guiCoords
	 * @return
	 */
	protected Vector2 transformGuiCoordToGameCoord(final Vector2 guiCoords) {
		Vector2 p = uiStage.stageToScreenCoordinates(guiCoords);
		p =  gameStage.screenToStageCoordinates(p);
		return p;
	}
	
	/**
	 * unused
	 * @param gameCoords
	 * @return
	 */
	public Vector2 transformGameCoordToGuiCoord(final Vector2 gameCoords) {
		Vector2 p = gameStage.stageToScreenCoordinates(gameCoords);
		p =  uiStage.screenToStageCoordinates(p);
		return p;
	}
	
    @Override
    public void resize(int width, int height) {
    	gameStage.getViewport().update(width, height);

    }

    @Override
    public void pause() {
    	setPaused(true);
    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        uiStage.dispose();
        gameStage.dispose();
    }

	private void setPaused(boolean paused) {
		this.paused = paused;
		if (paused)
		{
			A.getSound(SoundAsset.ICE_SPRAY).pause();
		}
		else
		{
			A.getSound(SoundAsset.ICE_SPRAY).resume();
		}
	}


    
    
    
    
    
    // ***************** Gesture detection methods below
    






	private float initialZoom =0;
	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		initialZoom =camera.zoom;
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
        camera.position.x -= (deltaX);
        camera.position.y += (deltaY);
        //Gdx.app.log("pan","distance Value:"+ deltaY);
		return true;
	}
	
	@Override
	public boolean zoom(float initialDistance, float distance) {
        float ratio = initialDistance / distance;
        camera.zoom = initialZoom * ratio;
        //Gdx.app.log("zoom", initialDistance +"distance Value:"+ distance);
        return true;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		return false;
	}

	@Override
	public void pinchStop() {
	}


}
