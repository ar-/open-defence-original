/*******************************************************************************
 * Copyright (C) 2015-2017 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.screens;

import com.ardash.opendefence.A;
import com.ardash.opendefence.A.ARAsset;
import com.ardash.opendefence.A.MusicAsset;
import com.ardash.opendefence.A.TextureAsset;
import com.ardash.opendefence.GameManager;
import com.ardash.opendefence.helpers.SoundPlayer;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

public abstract class GenericScreen implements Screen{

	public static float SCREEN_WIDTH = 1920;
	public static float SCREEN_HEIGHT = 1080; // full HD gui ratio
	protected Stage guiStage;
	private Stage backgroundStage;
	protected OrthographicCamera guiCam;
	protected Table rootTable;
	protected VerticalGroup contentVGroup;
//	protected GameManager gm;
	protected LabelStyle menustyle;

	public GenericScreen() {
		super();
	}

	@Override
	public void show() {
		backgroundStage = new Stage();
		Table table = new Table();
		table.setFillParent(true);
		table.setDebug(true);
        Image background = new Image(A.getTexture(TextureAsset.MENUBACK));
		backgroundStage.addActor(background);
        background.setAlign(Align.center);
        background.setScaling(Scaling.fill);
        background.setWidth(Gdx.graphics.getWidth());

        SoundPlayer.playMusic(A.getMusic(MusicAsset.INDUSTRIAL_96));
	    guiStage = new Stage(new ExtendViewport(SCREEN_WIDTH, SCREEN_HEIGHT));
        Gdx.input.setInputProcessor(guiStage);

		rootTable = new Table();
		rootTable.setFillParent(true);
		rootTable.setDebug(true);
		guiStage.addActor(rootTable);

//	    gm.getInputMultiplexer().clear();
//	    gm.getInputMultiplexer().addProcessor(guiStage);
	
	    // register with game manger
//		gm.currentGameScreen=null;
	    
	    // add GUI actors to stage, labels, meters, buttons etc.  
	    buildGameGUI();
	
	}

	/**
	 * 
	 */
	protected void buildGameGUI() {
	        Image badgeGpl3 = new Image(A.getTextureRegion(ARAsset.GPL3));
	        contentVGroup = new VerticalGroup();
	        rootTable.row().expandY();
	        rootTable.add(contentVGroup).center();	    
	        rootTable.row();
//	        rootTable.add().expandX();
	        rootTable.add(badgeGpl3).bottom().right().pad(30f).expandX();
	        Image badgeForkMe = new Image(A.getTextureRegion(ARAsset.FORK_ME));
	        badgeForkMe.setPosition(0, 0);
	        guiStage.addActor(badgeForkMe);

	        
		}

	@Override
	public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);  
	    Gdx.gl.glEnable(GL20.GL_TEXTURE_2D);  

	    backgroundStage.act(delta); // update GUI  
	    guiStage.act(delta); // update GUI  
	    
	    if (GameManager.renderGuiDebug)
	    {
	    	guiStage.setDebugAll(true);
	    }
	    backgroundStage.getViewport().apply();
	    backgroundStage.draw(); // draw the GUI  
	    guiStage.getViewport().apply();
	    guiStage.draw(); // draw the GUI  
	    
	}

	@Override
	public void resize(int width, int height) {
		// pass true because camera is unchanged on this UI stage
		backgroundStage.getViewport().update(width, height, true);
		guiStage.getViewport().update(width, height, true);
	}

	public Stage getGuiStage() {
		return guiStage;
	}

	public OrthographicCamera getGuiCam() {
		return guiCam;
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		// TODO with the current implementation a screen should be disposed after hiding
		//dispose();
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		guiStage.dispose();
		
	}

}
