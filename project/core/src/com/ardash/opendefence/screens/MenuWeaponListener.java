/*******************************************************************************
 * Copyright (C) 2017, 2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.screens;

import java.lang.reflect.Constructor;

import com.ardash.opendefence.P;
import com.ardash.opendefence.turrets.Turret;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class MenuWeaponListener extends InputListener {
	/**
	 * 
	 */
	private final GameScreen gameScreen;
	private final String turretTypeName;
	private Turret newTurret = null;
	boolean canDrop = false;
	private Constructor<?> constructor;
	
	/**
	 * @param gameScreen
	 */
	MenuWeaponListener(GameScreen gameScreen, String turretType) {
		this.gameScreen = gameScreen;
		this.turretTypeName = turretType;
	}

	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
		canDrop = false;
		// unselect current selection
		gameScreen.gameStage.setCurrentSelectedTurret(null);
		newTurret = createNewTurret(turretTypeName);
		
		// x,y is local for the actor (or its parent), make it global coords
		Vector2 v = P.getVector2(x, y);
		v = event.getListenerActor().localToStageCoordinates(v);
		newTurret.setPosition(gameScreen.transformGuiCoordToGameCoord(v));
		newTurret.setCursorColor(Turret.CURSOR_COLOR_RED);
		newTurret.setSelected(true);
		gameScreen.gameStage.addTurret(newTurret);
		gameScreen.validDropArea.setVisible(true);
		P.free(v);
		return true;
	}

	@Override
	public void touchDragged(InputEvent event, float x, float y, int pointer) {
		Vector2 v = P.getVector2(x, y);
		v = event.getListenerActor().localToStageCoordinates(v);
		newTurret.setPosition(gameScreen.transformGuiCoordToGameCoord(v));
		newTurret.getStage().showRadar(newTurret);
		canDrop = gameScreen.gameStage.isInValidArea(newTurret);
		
		// don't allow drop too close to the button
		if (y<60)
			canDrop = false;
		
		if (canDrop)
		{
			newTurret.setCursorColor(Turret.CURSOR_COLOR_GREEN);
		}
		else
		{
			newTurret.setCursorColor(Turret.CURSOR_COLOR_RED);
		}
		
		P.free(v);
		//Gdx.app.debug("touchDragged", "candrop: "+canDrop);
	}

	@Override
	public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
		super.touchUp(event, x, y, pointer, button);
		gameScreen.validDropArea.setVisible(false);
		newTurret.setCursorColor(Turret.CURSOR_COLOR_GRAY);
		newTurret.setSelected(false);
		if (!canDrop)
		{
			gameScreen.gameStage.removeTurret(newTurret);
		}
		else
		{
			newTurret.drop();
		}
		
		// remove all red collision markers that might be remaining
		for (Turret t : gameScreen.gameStage.getTurrets()) {
			t.hideCursor();
		}
	}
	
    private Turret createNewTurret(String className)
    {
    	try {
    		if (constructor == null)
    			constructor = Class.forName(className).getConstructor();
			return (Turret)constructor.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
    }

}