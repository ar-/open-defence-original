/*******************************************************************************
 * Copyright (C) 2017,2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.screens;


import com.ardash.opendefence.A;
import com.ardash.opendefence.A.ARAsset;
import com.ardash.opendefence.A.TextureAsset;
import com.ardash.opendefence.OpenDefenceGame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class TitleScreen extends GenericScreen {

    private OpenDefenceGame game;

    public TitleScreen(OpenDefenceGame aGame) {
        game = aGame;
    }
    
    @Override
    protected void buildGameGUI() {
    	super.buildGameGUI();

        Label title = new Label("OPEN DEFENCE", A.LabelStyleAsset.HEADLINE.style);
        title.setAlignment(Align.center);
        title.setHeight(SCREEN_HEIGHT/10f);
        contentVGroup.addActor(title);

        HorizontalGroup buttonRow1 = new HorizontalGroup();
        buttonRow1.pad(30);
        buttonRow1.addActor(makeMapSelectionButton("map_r01"));
        buttonRow1.addActor(makeMapSelectionButton("map_r02"));
        buttonRow1.addActor(makeMapSelectionButton("map_r03"));
        contentVGroup.addActor(buttonRow1);
//        buttonRow1.setWidth(Gdx.graphics.getWidth());
        buttonRow1.align(Align.center);
        buttonRow1.layout();
//        buttonRow1.setPosition(0,Gdx.graphics.getHeight()/2);
        
    }
    

	private TextButton makeMapSelectionButton(final String mapToLoad) {
		final String label = game.gm.peekMap(mapToLoad).label;
		TextButton playButton = new TextButton("Play\n"+label,A.TextButtonStyleAsset.MAP_SELECT.style);
//		TextButton playButton = new TextButton("Play\n"+label,A.getSkin(),"round");
		Image thumb = new Image(A.getTextureRegion("thumb_"+mapToLoad, false));
		thumb.setScaling(Scaling.fit);
		playButton.add(thumb);
//        playButton.setWidth(Gdx.graphics.getWidth()/2);
//        playButton.setHeight(500); 
        playButton.layout();
		playButton.align(Align.top);
		playButton.getLabel().setAlignment(Align.bottom);
		playButton.getLabelCell().expand(0, 0).bottom();
		
		VerticalGroup vg = new VerticalGroup();
		vg.addActor(thumb);
		vg.addActor(playButton.getLabel());
		playButton.add(vg).pad(10);
		playButton.pad(20f);
		
        //playButton.setPosition(Gdx.graphics.getWidth()/2-playButton.getWidth()/2,Gdx.graphics.getHeight()/2-playButton.getHeight()/2);
        playButton.addListener(new InputListener(){
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                game.setScreen(new GameScreen(game,mapToLoad));
            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
		return playButton;
	}

////    @Override
////    public void show() {
//////    	SoundPlayer.playMusic(A.getMusic(MusicAsset.INDUSTRIAL_96));
//////        Gdx.input.setInputProcessor(stage);
////    }
//
////    @Override
////    public void render(float delta) {
//////        Gdx.gl.glClearColor(1, 1, 1, 1);
//////        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
//////        stage.act();
//////        stage.draw();
////    }
//
////    @Override
////    public void resize(int width, int height) {
////
////    }
//
////    @Override
////    public void pause() {
////
////    }
////
////    @Override
////    public void resume() {
////
////    }
////
////    @Override
////    public void hide() {
////
////    }
//
////    @Override
////    public void dispose() {
////        stage.dispose();
////    }
}
