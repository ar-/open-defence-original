package com.ardash.opendefence.screens.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;

public class ControlButton extends WidgetGroup{
	private Label frame;
    final Image img;
	
	public ControlButton(Skin skin, AtlasRegion icon) {
        frame = new Label("\n", skin, "title");
		img = new Image(icon);
		addActor(frame);
		addActor(img);
		setSize(80, 80);
		img.setColor(Color.WHITE);
	}
	
	@Override
	public void setSize(float width, float height) {
		super.setSize(width, height);
		frame.setSize(width, height);
		img.setPosition( this.getWidth() / 2f - img.getWidth() / 2f, this.getHeight() / 2f - img.getHeight() / 2f);
	}
	
	public void makeLookActiveSelection(boolean b) {
		if (b) {
			img.setColor(Color.GOLD);
		}
		else {
			img.setColor(Color.WHITE);
		}
	}

}
