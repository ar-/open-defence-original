/*******************************************************************************
 * Copyright (C) 2017 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.screens.gui;

import com.ardash.opendefence.A;
import com.ardash.opendefence.OpenDefenceGame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;

public class FpsIndicator extends Label {

	public FpsIndicator(OpenDefenceGame game) {
		super("fps", A.getSkin(), "title");
        setAlignment(Align.left);
        setY(Gdx.graphics.getHeight()*2/3);
        setWidth(100);
	}

	@Override
	public void act(float delta) {
		setText(Gdx.graphics.getFramesPerSecond()+ " fps");
	}
}
