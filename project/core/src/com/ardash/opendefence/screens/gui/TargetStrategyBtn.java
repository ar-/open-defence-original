package com.ardash.opendefence.screens.gui;

import com.ardash.opendefence.turrets.TargetStrategy;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class TargetStrategyBtn extends TextButton {
	protected final TargetStrategy targetStrategy;
	protected TargetStrategyBtn(String text, Skin skin, TargetStrategy strategy)
	{
		super(text, skin, "toggle");
		targetStrategy = strategy;
	}
}
