package com.ardash.opendefence.screens.gui;

import com.ardash.opendefence.A.SoundAsset;
import com.ardash.opendefence.GameManager;
import com.ardash.opendefence.helpers.SoundPlayer;
import com.ardash.opendefence.turrets.TargetStrategy;
import com.ardash.opendefence.turrets.Turret;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class TurretWindow extends Window {
	
	private ButtonGroup<TargetStrategyBtn> targetStrategyBtnGroup;
	TextButton btnSell;
	TextButton btnUpgrade;
	Slider progressBar;
	private Label lblType;
	private Label lblDamage;
	private Label lblRange;
	private Label lblRate;
	private Label lblBlastRadius;
	private Label lblRotSpeed;
	private Label lblUpdateTime;

	private class TargetStrategyChangeListener extends ClickListener
	{
		@Override
		public void clicked(InputEvent event, float x, float y) {
			super.clicked(event, x, y);
			onTargetStrategyChanged();
		}
	}
	
	public TurretWindow(Skin skin) {
		super("", skin);
		final float width = 400;
		final float heigth = 550;
		setSize(width, heigth);
		setX(Gdx.graphics.getWidth()-width);
		setY(Gdx.graphics.getHeight()/2-heigth/2);

		final float stackHeight = buildUi(skin);
//		setHeight(stackHeight);
		setColor(1, 1, 1, 0f); // faded out
		setDebug(GameManager.renderGuiDebug);
		addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				// don't let clicks go through to the map, if visible
				return true;
			}
		});
		
	}
	
	private float buildUi(Skin skin) {
		VerticalGroup vg = new VerticalGroup();
		addActor(vg);
		vg.space(5);
		vg.fill(0.9f);
		vg.expand();
		vg.setDebug(GameManager.renderGuiDebug);
		vg.setFillParent(true);
		lblType = getTitleLabel();
		Label lblSpacer = new Label("\n\n", skin, "default");
		lblDamage = new Label("Damage", skin, "default");
		lblRange = new Label("Range", skin, "default");
		lblRate = new Label("Rate of Fire", skin, "default");
		lblBlastRadius = new Label("Blast Radius", skin, "default");
		lblRotSpeed = new Label("Rotation Speed", skin, "default");
		lblUpdateTime = new Label("Update time", skin, "default");
		
		// TODO increase font sizes
		btnSell = new TextButton("sell", skin);
		//progressBar = new ProgressBar(-1, 3, 1, false, skin);
		progressBar = new Slider(-1, 3, 0.001f, false, skin, "no-knob");
		//progressBar.getKnobDrawable();
		//progressBar.setColor(new Color(0, 0.2f, 0,1));
		progressBar.setValue(-1);
		progressBar.setDisabled(true);
		btnUpgrade = new TextButton("upgrade 50 $", skin);
		TargetStrategyBtn btnTargetNearest = new TargetStrategyBtn("target nearest", skin, TargetStrategy.NEAREST);
		btnUpgrade.setChecked(true);
		TargetStrategyBtn btnTargetFirst = new TargetStrategyBtn("target first", skin, TargetStrategy.FIRST);
		TargetStrategyBtn btnTargetStrongest = new TargetStrategyBtn("target strongest", skin, TargetStrategy.STRONGEST);
		TargetStrategyBtn btnTargetWeakest = new TargetStrategyBtn("target weakest", skin, TargetStrategy.WEAKEST);

		targetStrategyBtnGroup = new ButtonGroup<>();
		targetStrategyBtnGroup.add(btnTargetNearest, btnTargetFirst, btnTargetStrongest, btnTargetWeakest);
		for (TextButton b : targetStrategyBtnGroup.getButtons()) {
			b.addListener(new TargetStrategyChangeListener());
		}
		
		// configure sell-button
		btnSell.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				btnSell.setVisible(false);
				final Turret currentSelectedTurret = getStage().getCurrentSelectedTurret();
				currentSelectedTurret.getStage().gm.addMoney(currentSelectedTurret.getSellPrice()); // get the cash
				currentSelectedTurret.setSelected(false);
				currentSelectedTurret.getStage().removeTurret(currentSelectedTurret);
				//getStage().setCurrentSelectedTurret(null);
				SoundPlayer.playSound(SoundAsset.SP_TURRET_SOLD);
			}
		});
		
		// configure update-button
		btnUpgrade.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				btnUpgrade.setVisible(false);
				getStage().getCurrentSelectedTurret().startUpgrade();
				getStage().getCurrentSelectedTurret().setSelected(false);
				SoundPlayer.playSound(SoundAsset.SP_UPGRADE);
			}
		});
		
		vg.addActor(lblSpacer);
		vg.addActor(lblDamage);
		vg.addActor(lblRange);
		vg.addActor(lblRate);
		vg.addActor(lblBlastRadius);
		vg.addActor(lblRotSpeed);
		vg.addActor(lblUpdateTime);
		
		vg.addActor(btnSell);
		vg.addActor(progressBar);
		vg.addActor(btnUpgrade);
		vg.addActor(btnTargetNearest);
		vg.addActor(btnTargetFirst);
		vg.addActor(btnTargetStrongest);
		vg.addActor(btnTargetWeakest);

			vg.layout();
		return vg.getHeight();
		
	}
	
	/**
	 * this is called when a button in the targetStrategyBtnGroup was clicked
	 */
	private void onTargetStrategyChanged(){
		//Gdx.app.debug("TurretWindow", "onTargetStrategyChanged");
		final TargetStrategy targetStrategy = targetStrategyBtnGroup.getChecked().targetStrategy;
		getStage().getCurrentSelectedTurret().setTargetStrategy(targetStrategy);
		
		// play voice sound
		switch (targetStrategy) {
		case NEAREST:
			SoundPlayer.playSound(SoundAsset.SP_TARGET_NEAREST);
			break;
		case FIRST:
			SoundPlayer.playSound(SoundAsset.SP_TARGET_FIRST);
			break;
		case STRONGEST:
			SoundPlayer.playSound(SoundAsset.SP_TARGET_STRONGEST);
			break;
		case WEAKEST:
			SoundPlayer.playSound(SoundAsset.SP_TARGET_WEAKEST);
			break;
		default:
			break;
		}
	}
	
	void update() {
		final Turret turret = getStage().getCurrentSelectedTurret();
		
		// set typename, power, range, ....
		lblType.setText(turret.getClass().getSimpleName());
		lblDamage.setText("damage: " + turret.getDmg());
		lblRange.setText("range: " + (int)turret.getRange());
		lblRate.setText("Rate of Fire (s/sec): " + turret.getFirerate());
		lblBlastRadius.setText("Blast Radius: " + (int)turret.getBlastRadius());
		lblRotSpeed.setText("Rotation Speed (deg/sec): " + (int)turret.getRotSpeed());
		btnSell.setText("sell for " + turret.getSellPrice() + " $");
		btnSell.setVisible(true);

		if (turret.getMaxLevel() == turret.getLevel())
		{
			lblUpdateTime.setText("Fully Updated");
		}
		else
		{
			lblUpdateTime.setText("Update time: " + turret.getTimeToFinishUpdate());
		}
		
		// set the correct strategy button
		final TargetStrategy targetStrategy = turret.getTargetStrategy();
		for (TargetStrategyBtn btn : targetStrategyBtnGroup.getButtons()) {
			if (btn.targetStrategy == targetStrategy)
			{
				btn.setChecked(true);
				break;
			}
		}
		
		// set upgrade btn visibility
		btnUpgrade.setVisible(turret.isUpgradable());
		btnUpgrade.setText("upgrade "+ turret.getCost() +" $");
		
		// set progress bar
		final float pbMax = (float)turret.getMaxLevel();
		final float pbNow = (float)turret.getLevel();
		progressBar.setRange(-1f, pbMax);
		progressBar.setValue(pbNow);
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);
		final Turret turret = getStage().getCurrentSelectedTurret();
		if (turret != null)
		{
			final float upgradePercentage = turret.getUpgradePercentage();
			//Gdx.app.log("TW", "upgrade progress " + upgradePercentage);
			final float pbNow = turret.getLevel() + upgradePercentage -1.0f;
			progressBar.setValue(pbNow);
		}
	}

	@Override
	public void setVisible(boolean visible) {
		if (visible)
		{
			addAction(Actions.alpha(0.9f, 0.1f));
		}
		else
		{
			addAction(Actions.alpha(0.0f, 0.1f));
		}
	}
	
	/**
	 * visibility of this actor is arranged via alpha value.
	 * we need this override for the raytracing, so the touch event
	 * are not caught by an invisible window
	 */
	@Override
	public boolean isVisible() {
		return getColor().a > 0;
	}
	
	@Override
	public UiStage getStage() {
		return (UiStage)super.getStage();
	}

}
