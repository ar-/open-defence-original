/*******************************************************************************
 * Copyright (C) 2017, 2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.screens.gui;

import com.ardash.opendefence.DefenceStage;
import com.ardash.opendefence.common.Enemy;
import com.ardash.opendefence.turrets.Turret;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class UiStage extends Stage {
	private DefenceStage gameStage = null;
	private TurretWindow turretWindow = null;
	private Turret currentSelectedTurret = null;
	private ShapeRenderer healthBarRenderer;

	public UiStage(ScreenViewport screenViewport) {
		super(screenViewport);
		healthBarRenderer = new ShapeRenderer();
		healthBarRenderer.setAutoShapeType(true);
	}

	public TurretWindow getTurretWindow() {
		return turretWindow;
	}

	public void addTurretWindow(TurretWindow turretWindow) {
		this.turretWindow = turretWindow;
		addActor(turretWindow);
	}
	
	public void setCurrentSelectedTurret(Turret newSelectedTurret) {
		currentSelectedTurret = newSelectedTurret;
		if (currentSelectedTurret != null)
		{
			turretWindow.setVisible(true);
			turretWindow.update();// fill window
		}
		else
		{
			turretWindow.setVisible(false);
		}
	}
	
	public Turret getCurrentSelectedTurret() {
		return currentSelectedTurret;
	}

	public DefenceStage getGameStage() {
//		if (gameStage == null)
//			throw new RuntimeException("GameStage::getUiStage() must be called first");
		return gameStage;
	}
	
	/**
	 * will be set by the game stage when the stage link up each other
	 * @param s
	 */
	public void setGameStage(DefenceStage s) {
			gameStage = s;
	}

	@Override
	public void draw() {

		/*
		 * The lightnings change the blending function and were mostly the last thing to be drawn
		 * before the gui. If they are drawn directly before the gui it applies the same (wrong)
		 * blending to the health bars. Something must be drawn between lightnings and healthbars.
		 * Drawing the gui twice helps, but is inefficient. The following 3 lines do the trick.
		 */
		Batch batch = this.getBatch();
		batch.begin();
		batch.end();
		
		// health bars have been attached to the game stage actors, but the render order was wrong
		// instead of moving the actors to the ui stage and syncing the positions
		// we just draw them here manually, might be faster anyway using ShapeRenderer

		if (getGameStage()!=null)
		{
			final int hbHeight=8;
			final int hbWidth=60;
			final int hbFrame=1;
			final int screenHeight = Gdx.graphics.getHeight();
			healthBarRenderer.begin(ShapeType.Filled);
			for (Enemy e : gameStage.getEnemies()) {
				if (!e.isHealthBarVisible())
					continue;
				
				Vector2 center = e.getCenter();
				center = gameStage.stageToScreenCoordinates(center);
				
				// draw the frame of the healthbar
				healthBarRenderer.setColor(Color.BLACK);
				healthBarRenderer.rect(center.x, screenHeight-center.y, hbWidth, hbHeight);
				
				//draw the healthbar
				healthBarRenderer.setColor(e.getHealthBarColour());
				final int healthWidth = (int)((hbWidth-hbFrame*2) * e.getHealthPercentage());
				healthBarRenderer.rect(center.x+hbFrame, screenHeight-(center.y-hbFrame), healthWidth, hbHeight-hbFrame*2);
				
			}
			healthBarRenderer.end();
		}
		// now draw the actual stage
		super.draw();
	}
	
	@Override
	public void dispose() {
		super.dispose();
		healthBarRenderer.dispose();
	}
}
