package com.ardash.opendefence.screens.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.utils.Align;

public class WeaponButton extends WidgetGroup{
	private Label frame;
    final Label label;
    final Image img;
	
	public WeaponButton(Skin skin, AtlasRegion icon, int price) {
        frame = new Label("\n", skin, "title");
        label = new Label(""+price, skin, "title-plain");
        label.setAlignment(Align.bottom);
		img = new Image(icon);
		addActor(frame);
		addActor(img);
		addActor(label);
		setSize(80, 80);
		label.setColor(Color.YELLOW);
	}
	@Override
	public void setSize(float width, float height) {
		super.setSize(width, height);
		frame.setSize(width, height);
		label.setSize(width, height);
		label.setY(label.getY()+5);
		img.setPosition( this.getWidth() / 2f - img.getWidth() / 2f, this.getHeight() - img.getHeight()-5 );
	}

}
