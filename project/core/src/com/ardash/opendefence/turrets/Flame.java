package com.ardash.opendefence.turrets;

import com.ardash.opendefence.A;
import com.ardash.opendefence.A.ParticleAsset;
import com.ardash.opendefence.screens.GameScreen;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;

public class Flame extends Group{
	Image tmpImg = new Image(new Texture("gun_lvl1_base.png"));
	private ParticleEffect effect;
	private float lastdelta;
	
	private float m_fboScaler = 1.0f;
//	private float m_fboScaler = 0.5f;
	private boolean m_fboEnabled = true;
	private FrameBuffer m_fbo = null;
	private TextureRegion m_fboRegion = null;

	
	public Flame() {
		addActor(tmpImg);
		
		//TextureAtlas particleAtlas = A.get(A.actorsAtlas); // load atlas with the particle assets in
		effect = new ParticleEffect(A.getParticleEffect(ParticleAsset.FIRE_ADDITIVE6));
		//effect.load(Gdx.files.internal("fire_additive6.p"), particleAtlas);
//		effect.load(Gdx.files.internal("fire_additive.p"), Gdx.files.internal("./"));
//		effect.load(Gdx.files.internal("fire_additive6.p"), Gdx.files.local("./"));
		effect.start();
		effect.setPosition(100,200);
	}
	
	@Override
	public void setPosition(float x, float y) {
		//super.setPosition(x, y);
		//Setting the position of the ParticleEffect
		effect.setPosition(x, y);
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);
		lastdelta = delta;
	}
	
	boolean emitting = true;
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		
		if (Gdx.input.justTouched())
			emitting =!emitting;
		
		final Array<ParticleEmitter> emitters = effect.getEmitters();
		if(emitting)
			emitters.first().durationTimer = 0;

		
	    int width = Gdx.graphics.getWidth();
	    int height = Gdx.graphics.getHeight();
	    Gdx.app.log("Flame", "parts "+ effect.getEmitters().first().getActiveCount());

	    if(m_fboEnabled)      // enable or disable the supersampling
	    {                  
	        if(m_fbo == null)
	        {
	            // m_fboScaler increase or decrease the antialiasing quality

//	            m_fbo = new FrameBuffer(Format.RGB565, (int)(width * m_fboScaler), (int)(height * m_fboScaler), false);
//	            m_fbo = new FrameBuffer(Format.RGBA8888, (int)(width * m_fboScaler), (int)(height * m_fboScaler), false);
	            m_fbo = new FrameBuffer(Format.RGBA8888, GameScreen.TEX_SIZE, GameScreen.TEX_SIZE, false);
	            m_fboRegion = new TextureRegion(m_fbo.getColorBufferTexture());
	            m_fboRegion.flip(false, true);
	        }

	        m_fbo.begin();
	        Gdx.gl.glClearColor(0, 0, 0, 0.0f);
	        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

	    }


	 // Adding these lines did the trick
	    Camera camera = new OrthographicCamera(m_fbo.getWidth(), m_fbo.getHeight());
	    camera.position.set(m_fbo.getWidth() / 2, m_fbo.getHeight() / 2, 0);
	    camera.update();
	    final Matrix4 projectionMatrix = batch.getProjectionMatrix().cpy();
	    batch.setProjectionMatrix(camera.combined);
	    super.draw(batch, parentAlpha);
	    
		//Updating and Drawing the particle effect
		//Delta being the time to progress the particle effect by, usually you pass in Gdx.graphics.getDeltaTime();
		//effect.draw(batch, lastdelta);
//	    final ParticleEmitter particleEmitter = emitters.first();
//		particleEmitter.setPremultipliedAlpha(false);
//		particleEmitter.setAdditive(true);
		
		effect.draw(batch, Gdx.graphics.getDeltaTime());
	    batch.setProjectionMatrix(projectionMatrix);

//		effect.getEmitters().first().durationTimer = 0;
		if (effect.isComplete())
			effect.start();
//		tmpImg.draw(batch, parentAlpha);

	    // this is the main render function
	    //my_render_impl();

		final Vector2 scrCoord = getStage().stageToScreenCoordinates(new Vector2(getX(),getY()));
	    if(m_fbo != null)
	    {
	        m_fbo.end();

//	        batch.begin(); 
//	        batch.setBlendFunction(GL20.GL_DST_COLOR, GL20.GL_SRC_ALPHA);
//	        batch.setBlendFunction(GL20.GL_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
//	        batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_);
	        
//	        batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA); // regular alpha blending
//	        batch.setBlendFunction(GL20.GL_ONE, GL20.GL_SRC_ALPHA);
	        batch.setBlendFunction(GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA); // pre-multiplied alpha
//	        batch.setBlendFunction(GL20.GL_ONE_MINUS_SRC_ALPHA, GL20.GL_SRC_ALPHA);
	         
	        
//	        batch.setBlendFunction(GL20.GL_DST_COLOR, GL20.GL_SRC_ALPHA);
//	        spotLight.draw(batch, parentAlpha);
	        
	        
//	        batch.setColor(1, 1, 1, 0.5f);
//	        batch.draw(m_fboRegion, 0, 0, width, height);
//	        batch.draw(m_fboRegion, getX(), getY(), 200, 00, width, height, 0.2f, 0.2f, -3.14f, true);//(m_fboRegion, 0, 0, width, height);
//	        batch.draw(m_fboRegion, 0, 0, 0, 0, width, height, 1f, 1f, 0f);//(m_fboRegion, 0, 0, width, height);
//	        batch.draw(m_fboRegion, 100, 100, 0, 0, 200, 200, 1f, 1f, 0f);//(m_fboRegion, 0, 0, width, height);
	        batch.draw(m_fboRegion, 0, 0, 0, 0, GameScreen.TEX_SIZE, GameScreen.TEX_SIZE, 1f, 1f, 0f);//(m_fboRegion, 0, 0, width, height);
	        
//	        batch.setBlendFunction(GL20.GL_SRC_ALPHA,GL20.GL_ONE_MINUS_SRC_ALPHA);
//	        batch.end();
	        
	        
	        //reset 
	        batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
	        
//	        if (GameManager.renderStageDebug)
//	        {
//	        	final Vector3 min = effect.getBoundingBox().min;
//	        	final Vector3 max = effect.getBoundingBox().max;
//	    		ShapeRenderer shapeRenderer = new ShapeRenderer();
//				shapeRenderer.begin(ShapeType.Line);
//				shapeRenderer.setColor(Color.RED);
//				shapeRenderer.rect(min.x, min.y, max.x - min.x, max.y - min.y);
//				shapeRenderer.end();
//
//	        }
	    }   
}
}
