/*******************************************************************************
 * Copyright (C) 2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.turrets;

import java.util.ArrayList;

import com.ardash.opendefence.A;
import com.ardash.opendefence.A.ARAsset;
import com.ardash.opendefence.A.SoundAsset;
import com.ardash.opendefence.beans.TTD;
import com.ardash.opendefence.effects.IceEffect;
import com.ardash.opendefence.helpers.SoundPlayer;

public class Freezer extends Turret {
	
//	private static final float SOUND_LENGTH = 4.16f/2f;

	private IceEffect iceEffect;

	private float stopShootingTimestamp;
	private float stopSoundTimestamp;
	
	protected static ArrayList<TTD> tcs = new ArrayList<>(4);

	private long soundId;

	private boolean isSoundPlaying;

	
	static {
		for (int i=0;i<4;i++)
		{
			tcs.add(new TTD());
			tcs.get(i).AOE=27.5510205f;
			
			tcs.get(i).cost = (i==0)?30:tcs.get(i-1).cost*2;
			tcs.get(i).dmg = 1; // this turret slows down
			tcs.get(i).rate = 2;
			tcs.get(i).rotSpeed = (i==0)?90:tcs.get(i-1).rotSpeed*2;
		}
		
		tcs.get(0).range=125;
		tcs.get(1).range=166;
		tcs.get(2).range=208;
		tcs.get(3).range=250;
		
	}

	public Freezer() {
		super(A.getTextureRegion(ARAsset.GUN_LVL0_BASE), 
				A.getTextureRegions(ARAsset.FREEZER_TOP), 35);
		isSoundPlaying = false;
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);
		if (isDropped)
		{
			if (stopShootingTimestamp<=lifeTime)
			{
				iceEffect.stopEffect();
			}
			if (stopSoundTimestamp<=lifeTime)
			{
				SoundPlayer.stopSound(A.getSound(SoundAsset.ICE_SPRAY), soundId);
				isSoundPlaying = false;
			}

			iceEffect.setRotation(getAimDirection());
		}
	}


	@Override
	public void drop() {
		super.drop();
		iceEffect = new IceEffect();
		getStage().grp08bigParticle.addActor(iceEffect);
	}
	
	@Override
	public boolean remove() {
		SoundPlayer.stopSound(A.getSound(SoundAsset.ICE_SPRAY), soundId);
		getStage().grp08bigParticle.removeActor(iceEffect);
		return super.remove();
	}
	
	@Override
	protected void shoot() {
		//Gdx.app.log("Freezer", "shoot()");
		iceEffect.startEffect();
		iceEffect.setPosition(getCenterX(), getCenterY());
		iceEffect.setRange(getRange());
		
		if (!isSoundPlaying)
		{
			soundId = SoundPlayer.playSound(A.getSound(SoundAsset.ICE_SPRAY), true);
			isSoundPlaying = true;
		}
		stopSoundTimestamp=lifeTime+3.1f;
		
		stopShootingTimestamp=lifeTime+0.1f;
		
		currentTarget.freezeTemporarily(1.1f);
		getStage().freezeSurroundingEnemies(this, currentTarget, getRange2());
	}

	@Override
	protected ArrayList<TTD> getConfig() {
		return tcs;
	}
	
	public static int getBaseCost()
	{
		return tcs.get(0).cost;
	}

}
