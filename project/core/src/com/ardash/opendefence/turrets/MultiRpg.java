package com.ardash.opendefence.turrets;


public class MultiRpg extends Rpg {
	
	int currentRocketInBatch=1;
	
	@Override
	protected boolean isReloading() {
		// this must be done, otherwise the turret doesn't shoot in the first 2 secs of it's lifetime
		if (lastShotTimestamp==0)
			return false;
		
		final float shotsPerSecond = getFirerate();
		final float secondsBetweenShots = 1f / shotsPerSecond;
		final float secondsSinceLastShot = lifeTime - lastShotTimestamp;
		
		// shoots rockets in batches, with a delay of 0.1 between rockets in the same batch
		// and the rest of secondsBetweenShots (from firerate) between batches
		final int rocketsPerBatch = getRocketsPerBatch();
		final float delayBetweenRockets = 0.28f;
		final float delayUntilEndOfBatch = secondsBetweenShots*rocketsPerBatch - delayBetweenRockets*rocketsPerBatch;
		
		float secondsBetweenShots2 = delayBetweenRockets;
		if (currentRocketInBatch>rocketsPerBatch)
		{
			// last rocket in batch exceeded
			currentRocketInBatch=1;
		}
		if (currentRocketInBatch==1)
		{
			// last rocket in batch
			secondsBetweenShots2 = delayUntilEndOfBatch;
		}
			

//		Gdx.app.debug("Turret", "rocketsPerBatch: " + rocketsPerBatch );
//		Gdx.app.debug("Turret", "currentRocketInBatch: " + currentRocketInBatch );
//		Gdx.app.debug("Turret", "delayUntilEndOfBatch: " + delayUntilEndOfBatch );
//		Gdx.app.debug("Turret", "secondsBetweenShots2: " + secondsBetweenShots2 );
//		Gdx.app.debug("Turret", "------------------------: " );
//		Gdx.app.debug("Turret", "secondsBetweenShots: " + secondsBetweenShots );
//		Gdx.app.debug("Turret", "secondsSinceLastShot: " + secondsSinceLastShot );
//		Gdx.app.debug("Turret", "is reloading: " + (secondsSinceLastShot<secondsBetweenShots) );
		return (secondsSinceLastShot<secondsBetweenShots2);
	}
	
	@Override
	protected void shoot() {
		final float shotsPerSecond = getFirerate();
		final float secondsBetweenShots = 1f / shotsPerSecond;
		final float secondsSinceLastShot = lifeTime - lastShotTimestamp;
		final int rocketsPerBatch = getRocketsPerBatch();
		final float delayBetweenRockets = 0.28f;
		final float delayUntilEndOfBatch = secondsBetweenShots*rocketsPerBatch - delayBetweenRockets*rocketsPerBatch;

		// reset batch if last shot was very long ago
		if (secondsSinceLastShot>delayUntilEndOfBatch)
		{
			currentRocketInBatch=1;
		}
		super.shoot();
		currentRocketInBatch++;
	}
	
	private int getRocketsPerBatch()
	{
		return (getLevel()+1)*2;
	}

}
