/*******************************************************************************
 * Copyright (C) 2017,2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.turrets;

import com.ardash.opendefence.A;
import com.ardash.opendefence.A.ARAsset;
import com.ardash.opendefence.A.SoundAsset;
import com.ardash.opendefence.DefenceStage;
import com.ardash.opendefence.P;
import com.ardash.opendefence.actions.FlyTo;
import com.ardash.opendefence.common.DamageType;
import com.ardash.opendefence.common.Enemy;
import com.ardash.opendefence.common.HasCenter;
import com.ardash.opendefence.effects.Explosion;
import com.ardash.opendefence.effects.SmokeTrail;
import com.ardash.opendefence.helpers.SoundPlayer;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Pool.Poolable;
import com.badlogic.gdx.utils.Pools;

public class Rocket extends Image implements Poolable, HasCenter{
	
	private Enemy enemy;
	private int damage;
	private float blastRadius;
	private Sound sound;
	private long soundId;
	
	public Rocket() {
		super(A.getTextureRegion(ARAsset.ROCKET));
		setOrigin(getWidth()/2f, getHeight()/2f);
	}
	
    /**
     * Initialize the rocket. Call this method after getting a rocket from the pool.
     */
	public void init(float x, float y, Actor source, Enemy aEnemy, int dmg, float aBlastRadius)
	{
		this.enemy = aEnemy;
		this.damage = dmg;
		this.blastRadius = aBlastRadius;
		setPosition(x-getOriginX(), y-getOriginY());
		setRotation(source.getRotation());
		((DefenceStage)source.getStage()).grp09projectiles.addActor(this);
		// TODO pool the  action
		addAction(new FlyTo(aEnemy, 360*1.5f, 300/1.5f));
		setVisible(true);
		aEnemy.addAnnouncedDamage(damage);
		
		// play sound
		if (MathUtils.randomBoolean())
		{
			sound = A.getSound(SoundAsset.MISSILE3);
			soundId = SoundPlayer.playSound(sound);
		}
		else
		{
			sound = A.getSound(SoundAsset.MISSILE3);
			soundId = SoundPlayer.playSound(sound);
		}

	}

    /**
     * Callback method when the object is freed. It is automatically called by Pool.free()
     * Must reset every meaningful field of this rocket.
     */
	@Override
	public void reset() {
		setVisible(false);
		if (getActions().size >0)
		{
			removeAction(getActions().get(0));
		}
		getActions().clear();
		remove();
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);
		
		// smoke trace
		for (int i=0; i<3 ; i++)
		{
			SmokeTrail e = Pools.get(SmokeTrail.class).obtain();
			e.init(getStage(),getCenterX()+MathUtils.random(-3f,3f),getCenterY()+MathUtils.random(-3f,3f));
		}
		
		// cause damage and remove if target hit
		final DefenceStage stage = getStage();
		final float distance = stage.getDistance2(this, enemy);
		
		final float hitRadius = enemy.getHitRadius2();
		if (distance <hitRadius)
		{
			// hit detected
			//SoundPlayer.stopSound(sound, soundId); // TODO make rocket launch sound shorter
			enemy.damage(damage, DamageType.EXPLOSION);
			enemy.addAnnouncedDamage(-damage);
			stage.dealBlastDamage(this, enemy, damage, blastRadius);
			
			// display mini explosion (must be done before remove, otherwise: stage == null)
			// it looks weird to have the tiny explosion next to the enemy explosions
			// it can be removed but only if enemy is non-human (has its own explosion)
			Explosion ex = Pools.get(Explosion.class).obtain();
			ex.init(getStage(),getCenterX(),getCenterY(),0.5f,Explosion.MINI_EXPLOSION_RADIUS);

			Pools.get(Rocket.class).free(this);
		}
		
	}
	
	@Override
	public DefenceStage getStage() {
		return (DefenceStage)super.getStage();
	}
	
	@Override
	public float getCenterX()
	{
		return getX()+getOriginX();
	}
	
	@Override
	public float getCenterY()
	{
		return getY()+getOriginY();
	}
	
	@Override
	public Vector2 getCenter()
	{
		return P.getVector2(getCenterX(),getCenterY());
	}


}
