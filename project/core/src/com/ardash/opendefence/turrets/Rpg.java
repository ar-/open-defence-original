/*******************************************************************************
 * Copyright (C) 2017 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.turrets;

import java.util.ArrayList;

import com.ardash.opendefence.A;
import com.ardash.opendefence.A.ARAsset;
import com.ardash.opendefence.actions.RocketShotAction;
import com.ardash.opendefence.beans.TTD;

public class Rpg extends Turret {
	
	protected static ArrayList<TTD> tcs = new ArrayList<>(4);

	static {
		for (int i=0;i<4;i++)
		{
			tcs.add(new TTD());
			tcs.get(i).AOE=10; // higher than 10 for rockets looks bad
			
			tcs.get(i).cost = (i==0)?50:tcs.get(i-1).cost*2;
			tcs.get(i).dmg = (i==0)?500:tcs.get(i-1).dmg*2;
			tcs.get(i).rotSpeed = (i==0)?90:tcs.get(i-1).rotSpeed*2;
		}
		tcs.get(0).range=720/4;
		tcs.get(1).range=792/4;
		tcs.get(2).range=871/4;
		tcs.get(3).range=958/4;

		tcs.get(0).rate=0.5f;
//		tcs.get(0).rate=10f;
		tcs.get(1).rate=0.7f;
		tcs.get(2).rate=0.9f;
		tcs.get(3).rate=1.1f;

//		tcs.get(0).dmg=500;
//		tcs.get(1).dmg=1000;
//		tcs.get(2).dmg=2000;
//		tcs.get(3).dmg=4000;
		
		tcs.get(0).blastradius=50f;
		tcs.get(1).blastradius=70f;
		tcs.get(2).blastradius=90f;
		tcs.get(3).blastradius=120f;
		
//		tcs.get(0).rotSpeed=90f;
//		tcs.get(1).rotSpeed=180f;
//		tcs.get(2).rotSpeed=270f;
//		tcs.get(3).rotSpeed=360f;
		
}

	public Rpg() {
		super(A.getTextureRegion(ARAsset.GUN_LVL0_BASE), 
				A.getTextureRegions(ARAsset.GATLING_TOP), 35); // 29
	}
	
	@Override
	protected void shoot() {
		// TODO pool the  action
		top.addAction(new RocketShotAction(currentTarget,getDmg(),getBlastRadius()));
	}

	@Override
	protected ArrayList<TTD> getConfig() {
		return tcs;
	}
	
	public static int getBaseCost()
	{
		return tcs.get(0).cost;
	}

}
