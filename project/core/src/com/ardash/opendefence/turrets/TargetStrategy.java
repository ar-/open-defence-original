package com.ardash.opendefence.turrets;

public enum TargetStrategy {
	NEAREST(""), FIRST(""), STRONGEST(""), WEAKEST("");

	TargetStrategy(String t)
	{
		text = t;
	}
	public final String text;
	
}
