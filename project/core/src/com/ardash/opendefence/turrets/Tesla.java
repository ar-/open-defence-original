/*******************************************************************************
 * Copyright (C) 2017,2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.turrets;

import java.util.ArrayList;

import com.ardash.opendefence.A;
import com.ardash.opendefence.A.ARAsset;
import com.ardash.opendefence.actions.TeslaShotAction;
import com.ardash.opendefence.beans.TTD;

public class Tesla extends Turret {
	
	protected static ArrayList<TTD> tcs = new ArrayList<>(4);

	static {
		for (int i=0;i<4;i++)
		{
			tcs.add(new TTD());
			tcs.get(i).AOE=360;
			
			tcs.get(i).cost = (i==0)?60:tcs.get(i-1).cost*2;
			tcs.get(i).dmg = (i==0)?30:tcs.get(i-1).dmg*2;
			tcs.get(i).rotSpeed = 360;
			
			// lightning look best with constant rate, so increase only dmg
			tcs.get(i).rate=7f;
		}

		tcs.get(0).range=500/4;
		tcs.get(1).range=550/4;
		tcs.get(2).range=605/4;
		tcs.get(3).range=665/4;
		
	}

	public Tesla() {
		super(A.getTextureRegion(ARAsset.TESLA_BASE), 
				A.getTextureRegion(ARAsset.TESLA_BASE), 0);
		top.setVisible(false); // Tesla has no rotating top
	}
	
	
	
	@Override
	protected void shoot() {
		// TODO pool the action
		addAction(new TeslaShotAction(currentTarget,getDmg()));
	}

	@Override
	protected ArrayList<TTD> getConfig() {
		return tcs;
	}
	
	public static int getBaseCost()
	{
		return tcs.get(0).cost;
	}

}
