/*******************************************************************************
 * Copyright (C) 2017, 2018 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence.turrets;

import java.util.ArrayList;

import com.ardash.opendefence.A;
import com.ardash.opendefence.A.ARAsset;
import com.ardash.opendefence.A.SoundAsset;
import com.ardash.opendefence.DefenceStage;
import com.ardash.opendefence.P;
import com.ardash.opendefence.actions.AimTo;
import com.ardash.opendefence.beans.TTD;
import com.ardash.opendefence.common.Enemy;
import com.ardash.opendefence.common.HasCenter;
import com.ardash.opendefence.helpers.SoundPlayer;
import com.ardash.opendefence.screens.gui.UiStage;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;

public abstract class Turret extends Group implements HasCenter {
	//TODO finish flamer turret
	//TODO add laser turret
	//TODO add bonus drop weapons

	public static final Color CURSOR_COLOR_RED = new Color(1, 0, 0, 0.5f);
	public static final Color CURSOR_COLOR_GREEN = new Color(0, 1, 0, 0.5f);
	public static final Color CURSOR_COLOR_GRAY = new Color(0.5f, 0.5f, 0.5f, 0.5f);
	
	private Image base; // base
	protected Image top; // fixture , attachment
	
	/**
	 * If not null the upgrade procedure will show an upgraded texture for each level.
	 */
	private Array<AtlasRegion> upgradeTextures = null;
	
	private Image cursor;
	private Label upgradeLabel;
	
 	private ArrayList<Enemy> enemiesInRange = new ArrayList<>(20);
 	/**
 	 * nearest target in range
 	 */
	private Enemy nearestTarget = null;

 	/**
 	 * first (on the way to exit) target in range
 	 */
	private Enemy firstTarget = null;

 	/**
 	 * strongest target in range
 	 */
	private Enemy strongestTarget = null;

 	/**
 	 * weakest target in range
 	 */
	private Enemy weakestTarget = null;
	
	protected Enemy currentTarget = null;
	AimTo currentAimAction = null;
	protected float lifeTime = 0;
	protected float lastShotTimestamp = 0;
	private TargetStrategy targetStrategy = TargetStrategy.NEAREST;
	protected boolean isDropped =false;

	protected int level = -1; // indicates the current turret configuration
	final protected int MAX_LEVEL = 3;
	final private float cursorRadius;
	private float upgradeStartedTimestamp = Float.MIN_VALUE;
	
	public Turret(AtlasRegion texture, Array<AtlasRegion> toptextures, int turnOffsetY )
	{
		this(texture, toptextures.get(0), turnOffsetY );
		upgradeTextures = toptextures;
	}
	
	public Turret(AtlasRegion texture, AtlasRegion toptexture, int turnOffsetY )
	{
		base = new Image(texture);
		top = new Image(toptexture);
		cursor = new Image(A.getTextureRegion(ARAsset.CURSOR));
		addActor(base);
		addActor(top);
		addActor(cursor);
		base.setPosition(-base.getWidth()/2, -base.getHeight()/2);
		base.setOrigin( base.getWidth()/2, base.getHeight()/2);
		base.rotateBy(MathUtils.random(-20f, 20f));

		top.setPosition( -(top.getWidth() - turnOffsetY), -(top.getHeight()/2));
		top.setOrigin( top.getWidth() - turnOffsetY, top.getHeight()/2);
		top.rotateBy(MathUtils.random(-180f, 180f));
		
		cursorRadius =50;
		cursor.setSize(cursorRadius*2, cursorRadius*2);
		cursor.setPosition(-cursor.getWidth()/2, -cursor.getHeight()/2 );
		cursor.setOrigin(cursor.getWidth()/2, cursor.getHeight()/2);
		setCursorColor(CURSOR_COLOR_GRAY);
		setSelected(false);
		
		final Turret senderTurret = this;
		addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				if (!senderTurret.isDropped) // if someone right-clicks while dragging it in
					return false;
				// new selections go always through the stage, so it can keep the overview
				getStage().setCurrentSelectedTurret(senderTurret);
				// mark as handled, otherwise event goes through to map too and unselects
				return true;
			}
		});

		// verify config
		verify();
	}
	
	/**
	 * This should actually be a 'static abstract variable', which doesn't exist in Java.
	 * Poor language design:
	 * http://stackoverflow.com/questions/370962/why-cant-static-methods-be-abstract-in-java
	 * 
	 * This must always be implemented as : return tcs;
	 */
	protected abstract ArrayList<TTD> getConfig();
	
	@Override
	public void act(float delta) {
		super.act(delta);
		lifeTime+=delta;

		// if not dropped: bomb out here
		if (!isDropped)
			return;
		
		if (upgradeLabel == null)// && level == -1)
		{
			upgradeLabel = new Label("99%", A.getSkin(), "subtitle" );
			addActor(upgradeLabel);
			upgradeLabel.setColor(Color.YELLOW);
			//upgradeLabel.setFontScale(0.5f);
			return; // for isUpgrading() current timestamp must be higher
		}
		
		if (isUpgrading())
		{
			upgradeLabel.setText((int)(getUpgradePercentage() *100)+ "%");
			upgradeLabel.setVisible(true);
		}
		else
		{
			if (upgradeLabel.isVisible())
			{
				onUpgradeFinished();
			}
		}
		
		// if upgrade in progress: bomb out here
		if (isUpgrading())
		{
			return;
		}		
		
		// check if target still in range
		if (!isTargetInRange(currentTarget))
		{
			currentTarget = null;
			top.removeAction(currentAimAction);
			currentAimAction = null;
			selectNewTarget();
		}
		
		// try to select new target if no current target
		if (currentTarget == null)
		{
			selectNewTarget();
		}
		
		// if we arrive here, we have to rotate the top (aim)
		if (currentTarget != null)
		{
			if (top.getActions().size ==0)
			{
				currentAimAction = new AimTo(currentTarget,getRotSpeed());
				top.addAction(currentAimAction);
			}

			// if a target is in sight (in AOE) and alive now, we must shoot
			if ( isCurrentTargetInAOE() && ! isReloading() )
			{
				shoot();
				lastShotTimestamp = lifeTime;
			}
		}
	}

	/**
	 * implement this to ad the correct action of the shooting
	 */
	protected abstract void shoot();
	
	public void setPosition(Vector2 v) {
		this.setPosition(v.x, v.y);
	}
	
	@Override
	public DefenceStage getStage() {
		return (DefenceStage)super.getStage();
	}
	
	public UiStage getUiStage() {
		final DefenceStage stage = getStage();
		return stage == null ? null : stage.getUiStage();
	}
	
	public float getRange()
	{
		// before building we need the range to show the radar correctly
		if (level ==-1)
			return getConfig().get(0).range;
		return getConfig().get(level).range;
	}
	
	public float getRange2()
	{
		// TODO do this in initialisation
		return getRange() * getRange();
	}
	
	/**
	 * returns the Angle of Effect
	 * @return
	 */
	public float getAOE()
	{
		return getConfig().get(level).AOE;
	}
	
	/**
	 * returns fire rate in shots per second
	 * @return
	 */
	public float getFirerate()
	{
		return getConfig().get(level).rate;
	}
	
	public int getDmg()
	{
		return getConfig().get(level).dmg;
	}
	
	public float getBlastRadius()
	{
		return getConfig().get(level).blastradius;
	}

	public float getRotSpeed()
	{
		return getConfig().get(level).rotSpeed;
	}

	/**
	 * 
	 * @return cost for the next update
	 */
	public int getCost()
	{
		return getConfig().get(level).cost;
	}
	
	/**
	 * 
	 * @return cost for the next update
	 */
	public int getSellPrice()
	{
		float invested = getConfig().get(0).cost;
		for (int i=0; i<level ; i++)
		{
			invested += getConfig().get(i).cost;
		}
		return MathUtils.round(invested*0.7f);
	}
	
	/**
	 * 
	 * @return -1 if newly built, or the current level  from 0 to MAX_LEVEL
	 */
	public int getLevel()
	{
		return level;
	}

	/**
	 * 
	 * @return MAX_LEVEL for this turret (usually 3 for 4 levels)
	 */
	public int getMaxLevel()
	{
		return MAX_LEVEL;
	}

	/**
	 * Currently only in use for current target, to check if it got out of range.
	 * @param e
	 * @return
	 */
	private boolean isTargetInRange(Enemy e)
	{
		if (e == null)
			return false;
		// check if dead, almost dead, or left the map (went its whole way)
		if (!e.isAlive() || e.isDefinetelyGoingToDie() || e.getStage() == null)
			return false;
		float distance = getStage().getDistance2(this, e) - e.getHitRadius2();
		return (distance <= getRange2());
	}
	
	private boolean isCurrentTargetInAOE()
	{
		if (currentTarget == null)
			return false;
		if (!currentTarget.isAlive() || currentTarget.isDefinetelyGoingToDie())
			return false;
		if (currentAimAction == null)
			return false;
		final float ate = currentAimAction.getAngleToEnemy();
		return (Math.abs(ate)<=getAOE());
	}
	
	private void refreshEnemiesInRange()
	{
		enemiesInRange.clear();
		nearestTarget = null;
		firstTarget = null;
		strongestTarget = null;
		weakestTarget = null;
		float nearestDistance2 = Float.MAX_VALUE;
		float farestProgress = Float.MIN_VALUE;
		int lowestHP = Integer.MAX_VALUE;
		int highestHP = Integer.MIN_VALUE;
		final float range2 = getRange2();
		for (Enemy e : getStage().getEnemies()) {
			if (!e.isAlive() || e.isDefinetelyGoingToDie())
				continue;
			
			final float distance2 = getStage().getDistance2(this, e) - e.getHitRadius2();
			final float progress = e.getProgress();
			final int hp = e.getHP();
			
			
			if (distance2 <= range2)
			{
				enemiesInRange.add(e);
				//Gdx.app.log("Turret", "target in range, distance2 " +distance2);
				
				// Target is in range, save when it is nearest, farest, strongest, weakest , ....
				
				// save the nearest target
				if (distance2<nearestDistance2)
				{
					nearestTarget = e;
					nearestDistance2 = distance2;
				}
				
				//save most progressed target toward goal (first target)
				if (progress>farestProgress)
				{
					firstTarget = e;
					farestProgress = progress;
				}

				//save strongest target
				if (hp>highestHP)
				{
					strongestTarget = e;
					highestHP = hp;
				}

				//save weakest target
				if (hp<lowestHP)
				{
					weakestTarget = e;
					lowestHP = hp;
				}
			}
		}
	}
	
	private void selectNewTarget()
	{
		refreshEnemiesInRange();
		switch (targetStrategy) {
		case NEAREST:
			if (nearestTarget !=null)
			{
				currentTarget = nearestTarget;
			}
			break;
		case FIRST:
			if (firstTarget !=null)
			{
				currentTarget = firstTarget;
			}
			break;
		case STRONGEST:
			if (strongestTarget !=null)
			{
				currentTarget = strongestTarget;
			}
			break;
		case WEAKEST:
			if (weakestTarget !=null)
			{
				currentTarget = weakestTarget;
			}
			break;
		default:
			throw new IllegalArgumentException("unknown targetStrategy"+targetStrategy);
		}

		if (nearestTarget !=null)
		{
			//Gdx.app.debug("Turret", "selected new target " +currentTarget);
		}
		// TODO limit money to buy turrets to max amount
	}
	
	/**
	 * There is a break between the shots determined by the fire rate.
	 * The fire rate is shots per second.
	 * The break is called 'reloading'.
	 */
	protected boolean isReloading()
	{
		// this must be done, otherwise the turret doesn't shoot in the first 2 secs of it's lifetime
		if (lastShotTimestamp==0)
			return false;
		
		final float shotsPerSecond = getFirerate();
		final float secondsBetweenShots = 1f / shotsPerSecond;
		final float secondsSinceLastShot = lifeTime - lastShotTimestamp;
		//Gdx.app.debug("Turret", "secondsBetweenShots: " + secondsBetweenShots );
		//Gdx.app.debug("Turret", "secondsSinceLastShot: " + secondsSinceLastShot );
		//Gdx.app.debug("Turret", "is reloading: " + (secondsSinceLastShot<secondsBetweenShots) );
		return (secondsSinceLastShot<secondsBetweenShots);
	}
	
	/**
	 * Call this to drop a new turret onto the stage (this follows dragging).
	 */
	public void drop() {
		isDropped=true;
		startUpgrade();
	}
	
	public void startUpgrade() {
		upgradeStartedTimestamp = lifeTime;
		level++;
	}
	
	/**
	 * -1 -> 2
	 * 0 -> 4
	 * 1 -> 8
	 * 2 -> 16
	 * @return 2^(level+2)
	 */
	public int getTimeToFinishUpdate()
	{
		final int exp = level+2;
		return (int)Math.pow(2,exp);
	}
	
	private boolean isUpgrading() {
		float timeToFinishUpgrade= getTimeToFinishUpdate()/2; // /2 because lvl is already upped
		final float timestampOfFinishedUpgrade = upgradeStartedTimestamp+timeToFinishUpgrade;
		return (timestampOfFinishedUpgrade > lifeTime);
	}
	
	/**
	 * @return TRUE if the turret is upgradable right now
	 */
	public boolean isUpgradable() {
		if (isUpgrading())
			return false;
		if (level >= MAX_LEVEL)
			return false;
		return true;
	}

	public float getUpgradePercentage() {
		float timeToFinishUpgrade= getTimeToFinishUpdate()/2; // /2 because lvl is already upped
		final float upgradeRuntime = lifeTime - upgradeStartedTimestamp ;
		float res = upgradeRuntime / timeToFinishUpgrade;
		if (res > 1.0f)
			res = 1.0f;
		return res;
	}

	/**
	 * is being executed when an upgrade to the next level finished.
	 */
	private void onUpgradeFinished() {
		// upgrade has just finished
		upgradeLabel.setVisible(false);
		if (level > 0)
			SoundPlayer.playSound(SoundAsset.SP_UPGRADE_COMPLETE);
		
		// inform gui if this turret is selected
		final UiStage uiStage = getUiStage();
		if (uiStage.getCurrentSelectedTurret() == this)
		{
			uiStage.setCurrentSelectedTurret(this); // this will update the gui
		}
		
		// update radar size not needed as window closes after click update
		// getStage().showRadar(this);
		
		// change top texture to make the turret look cooler
		if (upgradeTextures != null)
		{
			// easy sanity check and quick fail
			if (upgradeTextures.size!=MAX_LEVEL+1)
			{
				throw new RuntimeException("Turret has "+(MAX_LEVEL+1)+" levels but texture region has " + upgradeTextures.size + " members");
			}
			
			top.setDrawable(new TextureRegionDrawable(new AtlasRegion(upgradeTextures.get(level))));
		}
	}

	@Override
	public float getCenterX()
	{
		return getX()+getOriginX();
	}
	
	@Override
	public float getCenterY()
	{
		return getY()+getOriginY();
	}
	
	@Override
	public Vector2 getCenter()
	{
		return P.getVector2(getCenterX(),getCenterY());
	}
	
	/**
	 * @return The rotation of the top of the turret (aim) in positive degrees.
	 */
	public float getAimDirection()
	{
		// add 2*360 to make value always positive
		// sub 180 because top looks always backwards
		float ret = top.getRotation()+(360f*2f)-180f;
		if (ret>360f)
			ret-=360f;
		return ret;
	}

	public float getCursorRadius() {
		return cursorRadius;
	}
	
	public TargetStrategy getTargetStrategy() {
		return targetStrategy;
	}

	public void setTargetStrategy(TargetStrategy targetStrategy) {
		this.targetStrategy = targetStrategy;
	}

	public void setCursorColor(Color c) {
		cursor.setColor(c);
	}

	public void setSelected(boolean sel) {
		cursor.setVisible(sel);
		
		// inform stage
		final DefenceStage stage = getStage();
		if (stage!=null)
		{
			if (sel)
				stage.showRadar(this);
			else
				stage.hideRadar();
		}
		
		// inform UI stage
		final UiStage uiStage = getUiStage();
		if (uiStage!=null)
		{
			if (sel)
				uiStage.setCurrentSelectedTurret(this);
			else
				uiStage.setCurrentSelectedTurret(null);
		}
	}
	
//	private boolean showRedCursorInNextFrame = false;
	public void showRedCursor() {
		cursor.setColor(CURSOR_COLOR_RED);
		cursor.setVisible(true);
//		showRedCursorInNextFrame = true;
	}
	
	public void hideCursor() {
		cursor.setColor(CURSOR_COLOR_GRAY);
		cursor.setVisible(false);
	}
	
	/**
	 * checks the configuration
	 */
	private void verify()
	{
		for (int i=0; i<MAX_LEVEL ; i++)
		{
			getConfig().get(i).verify();
		}
	}

}
