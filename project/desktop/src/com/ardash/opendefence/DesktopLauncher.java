/*******************************************************************************
 * Copyright (C) 2017 Andreas Redmer <andreasredmer@mailchuck.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.ardash.opendefence;

import com.ardash.opendefence.beans.Map;
import com.ardash.opendefence.beans.SpawnDescriptor;
import com.ardash.opendefence.beans.Wave;
import com.ardash.opendefence.beans.WaveList;
import com.ardash.opendefence.beans.Way;
import com.ardash.opendefence.beans.WayList;
import com.ardash.opendefence.beans.WayPoint;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings;
import com.badlogic.gdx.utils.Json;

public class DesktopLauncher {
	@SuppressWarnings("unused")
	public static void main (String[] arg) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
//		config.width=800;config.height=480; // galaxy S2 ratio:1.66
//		config.width=(int)(800*1.5);config.height=(int)(480*1.5); // galaxy S2 scaled ratio:1.66
//		config.width=1280;config.height=720; // galaxy HTC one X+ ratio:1.77
		config.setWindowedMode(1280, 720);
		config.setWindowedMode(1480, 720);
		config.setForegroundFPS(60);
		config.setTitle("Open Defence");
		
//		packTextures();

		new Lwjgl3Application(new OpenDefenceGame(), config);
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
		
		//runSelfTests();
	}

	private static void packTextures() {
		Settings settings = new Settings();
		settings.maxWidth = 512;
		settings.maxHeight = 512;
		settings.maxWidth = 1024;
		settings.maxHeight = 1024;
		TexturePacker.process(settings, "../../texturepack/actors", "../android/assets", "actors");
		TexturePacker.process(settings, "../../texturepack/gui", "../android/assets", "gui");
		settings.maxWidth = 2048;
		settings.maxHeight = 2048;
		TexturePacker.process(settings, "../../texturepack/explosion", "../android/assets", "explosion");
		
	}

//	private static void runSelfTests() {
//		//createExampleModels();
//		final OpenDefenceGame tmpgame = new OpenDefenceGame();
//		new LwjglApplication(tmpgame);
//		GameManager gm = new GameManager(tmpgame);
//		gm.loadMap("map_01");
//		assert gm != null;
//		final Map currentMap = gm.getCurrentMap();
//		final AssetManager am = new AssetManager();
//		am.finishLoadingAsset(gm.getCurrenttextureName());
//		assert am.isLoaded("sss");
//		assert false;
//		
//	}

	private static void createExampleModels() {
		//String dir = "../android/assets/conf/";
		String dir = "./conf/";
//		String fWay = dir+Way.class.getSimpleName()+".json";
//		String fWayList = dir+WayList.class.getSimpleName()+".json";
//		String fMap = dir+Map.class.getSimpleName()+".json";
		WayPoint wp0 = new WayPoint(400,300,5,"wp0");
		WayPoint wp1 = new WayPoint(450.5f,350.6f,5,"wp1");
		
		Way way1 = new Way();
		way1.wayPoints.add(wp0);
		way1.wayPoints.add(wp1);
		Way way2 = new Way(way1);
		Way way3 = new Way(way1);

		WayList wl = new WayList();
		wl.add(way1);
		wl.add(way2);
		wl.add(way3);
		
		SpawnDescriptor sd1 = new SpawnDescriptor(0,0.3f,10,"Tank",0);
		SpawnDescriptor sd2 = new SpawnDescriptor(5,0.5f,20,"Soldier",0);
		
		Wave wv = new Wave();
		wv.add(sd1);
		wv.add(sd2);
		
		WaveList wvl = new WaveList();
		wvl.add(wv);
		wvl.add(wv);
		
		Map map = new Map();
		map.wayList = wl;
		map.textureName = "whatever.png";
		map.waveList = wvl;
		
		Json json = new Json();
		json.setElementType(Map.class, "wayList", Way.class);
		//json.setOutputType(OutputType.json);
//		json.setSerializer(Wave.class, new Json.Serializer<Wave>() {
//
//			@Override
//			public void write(Json json, Wave object, Class knownType) {
//			      json.writeArrayStart();
//			      json.write
//			      json.writeField(object, "");
//			      json.writeValue(object.name, number.number);
//			      json.writeArrayEnd();
//			}
//
//			@Override
//			public Wave read(Json json, JsonValue jsonData, Class type) {
//				return null;
//			}
//
//		} );
//		System.out.println(json.prettyPrint(way1,200 ));
//		System.out.println(json.prettyPrint(way1,200 ) );
//		System.out.println(json.prettyPrint(wl,200 ));
		
//		ArrayList<Object> ol = new ArrayList<>();
//		ol.add(wp0);
//		ol.add(way1);
//		ol.add(wl);
//		ol.add(sd1);
//		ol.add(wv);
//		ol.add(wvl);
//		ol.add(map);
//		for (Object o : ol) {
//			Class clazz = o.getClass();
//			String filename = dir+clazz.getSimpleName()+".json";
////			System.out.println("==================");
////			System.out.println(filename);
////			System.out.println("==================");
//			String jsonLines = json.prettyPrint(o, 200);
//			//jsonLines = jsonLines.replaceAll("class: [A-Za-z\\.,]*", "");
////			System.out.println(jsonLines);
//			FileHandle fh = Gdx.files.local(filename);
//			fh.writeString(jsonLines, false);
////			final Object result = 
//					json.fromJson(clazz, Gdx.files.local(filename));
//			//System.out.println("cl "+clazz.getSimpleName()+ " "+ result);
//		}	
		

	}
}
