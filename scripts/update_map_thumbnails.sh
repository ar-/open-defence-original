#!/bin/bash
for baseimg in `ls project/android/assets/maps/*.jpg | egrep "[0-9]\.jpg"`
do
	nr=`echo $baseimg | egrep -o "[0-9]+\.jpg" | egrep -o "[0-9]+"`
	echo number $nr
	resultfile="texturepack/gui/thumb_map_r$nr.png"
	echo creating $resultfile
	echo found base $baseimg
	treeimg=`echo $baseimg | sed 's/\.jpg/_t\.png/'`
	if [ -e "$treeimg" ]; then
		echo found trees $treeimg
		#convert -composite project/android/assets/maps/map_r01.jpg project/android/assets/maps/map_r01_t.png -gravity center texturepack/gui/thumb_map_r01.png
		convert -composite $baseimg $treeimg -gravity center $resultfile
	else 
		echo found NO trees 
		convert $baseimg $resultfile
	fi 

	convert $resultfile -resize 128x128 $resultfile
	convert $resultfile -alpha set -virtual-pixel transparent -channel A -radial-blur 0x45 +channel $resultfile
done